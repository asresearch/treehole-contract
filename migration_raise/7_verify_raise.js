const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const THTokenRaise = artifacts.require("THTokenRaise")
const {DHelper, StepRecorder, SoEVerifier} = require("../util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  if(network.includes('ganache')){
    return;
  }
  //if(!network.includes('ropsten')){
    //return;
  //}

  rr =  StepRecorder(network, "raise")
  raise = await THTokenRaise.at(rr.read("raise"))
  start_block = rr.read("raise-start-block")
  end_block = rr.read("raise-end-block")
  amount = rr.read('raise-amount')
  share = rr.read('raise-share')
  usdc = rr.read('raise-fiat-token')
  pool = rr.read("pool")

  severifier =  SoEVerifier(network, "raise-verify")
  await severifier.verifyContractOnEtherScan(THTokenRaise, raise.address, [SafeMath], start_block, end_block, amount, usdc, share, pool)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
