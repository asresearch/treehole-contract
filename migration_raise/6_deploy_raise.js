const {DHelper, StepRecorder} = require("../util.js");
const TokenBankV2 = artifacts.require("TokenBankV2");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const THTokenRaise = artifacts.require("THTokenRaise")
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");

async function performMigration(deployer, network, accounts, dhelper) {
  common = StepRecorder(network, "raise-common")
  sr = StepRecorder(network, 'raise')
  tlist_factory = await TrustListFactory.at(common.read("tlist-factory"))
  //1. usdc
  if(network.includes('ropsten')){
    token_factory = await ERC20TokenFactory.at(common.read("token-factory"))
    tx = await tlist_factory.createTrustList([accounts[0]])
    token_trustlist = await TrustList.at(tx.logs[0].args.addr);
    sr.write("USDC tlist", token_trustlist.address);
    console.log("token factory: ", token_factory.address)
    tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000', 0, "USD Coin", 6, "USDC", true);
    token = await ERC20Token.at(tx.logs[0].args._cloneToken);
    await token.changeTrustList(token_trustlist.address);
    sr.write("USDC", token.address);
    await token.generateTokens(accounts[0], "10000000000000")
    usdc = token.address
  }else{
    usdc = "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48"
  }
  //2. start_block, end_block
  if(network.includes('ropsten')){
    start_block = 12289581
  }else{
    start_block = 12289581
  }
  end_block = start_block + 6400*180 //about 6 months
  //3. create pool
  bank_factory = await TokenBankV2Factory.at(common.read("bank-factory"))
  tx = await bank_factory.newTokenBankV2('TreeHole vault')
  bank = await TokenBankV2.at(tx.logs[0].args.addr);

  tx = await tlist_factory.createTrustList([accounts[0]])
  bank_tlist = await TrustList.at(tx.logs[0].args.addr)
  await bank.changeTrustList(bank_tlist.address)
  sr.write("pool", bank.address)
  sr.write("pool_tlist", bank_tlist.address)

  //4. create raise
  amount = "10000000000000"
  share =  "20000000000000000000000000"
  sr.write("raise-start-block", start_block)
  sr.write("raise-end-block", end_block)
  sr.write("raise-amount", amount)
  sr.write("raise-share", share)
  sr.write('raise-fiat-token', usdc)

  raise = await dhelper.readOrCreateContract(THTokenRaise, [SafeMath, SafeERC20], start_block, end_block, amount, usdc, share, bank.address)
  sr.write("raise", raise.address)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
