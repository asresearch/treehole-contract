const {DHelper, StepRecorder} = require("../util.js");
const TokenBankV2 = artifacts.require("TokenBankV2");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");

async function performMigration(deployer, network, accounts, dhelper) {
  f = await dhelper.readOrCreateContract(TokenBankV2Factory)
  common =  StepRecorder(network, "common")
  common.write("tokenbank-factory", f.address)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
