const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const MerkleProof = artifacts.require("MerkleProof");
const SGXRequest = artifacts.require("SGXRequest");
const SGXOnChainResult = artifacts.require("SGXOnChainResult");
const SGXStaticData = artifacts.require("SGXStaticData");
const ECDSA = artifacts.require("ECDSA");
const SignatureVerifier = artifacts.require("SignatureVerifier");
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXProgramStoreFactory = artifacts.require("SGXProgramStoreFactory");
const SGXStaticDataMarketPlaceFactory = artifacts.require("SGXStaticDataMarketPlaceFactory");
const SGXKeyVerifier = artifacts.require("SGXKeyVerifier");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const SGXProgramStore = artifacts.require("SGXProgramStore");
const SGXStaticDataMarketPlace = artifacts.require("SGXStaticDataMarketPlace");
const SGXOnChainResultMarket = artifacts.require("SGXOnChainResultMarket")
const SGXOnChainResultMarketImplV1 = artifacts.require("SGXOnChainResultMarketImplV1")
const SGXDataMarketCommon = artifacts.require("SGXDataMarketCommon");
const SGXDataMarketCommonImplV1 = artifacts.require("SGXDataMarketCommonImplV1")
const NaiveOwner = artifacts.require("NaiveOwner");

const {DHelper, StepRecorder, SoEVerifier} = require("../util.js");

async function performMigration(deployer, network, accounts, dhelper) {

  common = StepRecorder(network, "common")
  sr =  StepRecorder(network, "market")
  if(network.includes("main")){
    return ;
  }
  pstore = await SGXProgramStore.at(sr.read("program-store"))
  usdc = await ERC20Token.at(sr.read("payment token"))
  market = await SGXStaticDataMarketPlace.at(sr.read("market"))
  onchain_market = await SGXOnChainResultMarket.at(sr.read("onchain-market"))
  common_market = await SGXDataMarketCommon.at(sr.read("common-market"))

  console.log("1")
  tx = await pstore.upload_program("test_url", 0, "0x3e0d3a43f4f45ba7a1234759c2ffa4028a44599d4ab29bec532bd2057c0f9141")
  program_hash = tx.logs[0].args.hash
  dhash = "0x3e0d3a43f4f45ba7a1234759c2ffa4028a44599d4ab29bec532bd2057c0f9141"
  pkey = '0x362a609ab5a6eecafdb2289890bd7261871c04fb5d7323d4fc750f6444b067a12a96efbe24c62572156caa514657d4a535101d2147337f41f51fcdfcf8f43a53'
  let hash_sig = '0xe014387b04dddc1a2ec75b42f2f8313395e1c391a8fb3bd1544d82071e1c1ae665ce132eac6f6f57f900c1d781714bddd8297b770d11714aa6e5ad4bdd9516eb1b';
  console.log("2")
  tx = await common_market.createStaticData(dhash, "test env", 100, pkey, hash_sig);
  data_vhash = tx.logs[0].args.vhash

  let secret = '0x9fbe7febcd5c9bd7e50a51ca03652271fd0455a800bf331e02a85e6223e8493905e8282f68e70b56eef6e51a3c69f453d8c5dcdb3ac36144e5e810a41202a478d4a6b4d57db8bb6ea0931c5907037eaac6ed9980426479ec401157bd649bc33e3eefbec1354fb8aa4feca5f1681aeed1581c201ab7f73c636051f63f4221183f51f2c02591a47322cf57055e18e63aa246f5c6d9ab2c28b233b8d807b843e9111cc110dfdc8ffcf7ad8afffd4848832a84';
  let input = "0xaf4145ab19e5a354c2118032d4a6ca81ac4ddf1ffcd0e227cd648fb1701d95f917e31481e38d832d38f0ffbbba80228ab1ee9c05e88f997cae4354677f4fe0b9dce23ca07309cddc32dd0997517aec00687314";
  let forward_sig = '0x0ce588a6d240a4c6da1b9c887c32576fd4eb43b170d42d4fea01e8cdfc50be634fdce867af14b76f8fbade6ca03a42b74ea855a1f12e80ff71e2dd5f79879f6d1c';
  let gas_price = 10000000000;
  pkey = "0x5d7ee992f48ffcdb077c2cb57605b602bd4029faed3e91189c7fb9fccc72771e45b7aa166766e2ad032d0a195372f5e2d20db792901d559ab0d2bfae10ecea97"
  await usdc.approve(onchain_market.address, 0);
  await usdc.generateTokens(accounts[0], "10000000000000000000")
  await usdc.approve(onchain_market.address, "10000000000000000000")
  console.log("3")
  tx = await onchain_market.requestOnChain(data_vhash, secret, input, forward_sig, program_hash, gas_price, pkey,"10000000000000000000");
  request_hash = tx.logs[0].args.request_hash

  let result = '0x566df6fc662a2cd177ee05de6dde43e0b1aa2798866b1e1656ff5d2c38171a1e76de34448e9f527a04754942290a87e3c50de136d22c8b196e7db089f6edd8b2643830b1e8b8051ec75e55d4d8eabcce679b18c41aec0dad05df7e8c0e'
  let result_signature = '0xd83b1a6cddc99d5564c0a0dd38d66323055e86f6b226d5c588ac8d1fb09e5d056d41972873c41e286df1246cc667d6cf17a1699ec854921005304d6f0a02fc0f1b';
  console.log("4")
  tx = await onchain_market.submitOnChainResult(data_vhash, request_hash, 12876, result, result_signature);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
