const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const MerkleProof = artifacts.require("MerkleProof");
const SGXRequest = artifacts.require("SGXRequest");
const SGXOnChainResult = artifacts.require("SGXOnChainResult");
const SGXStaticData = artifacts.require("SGXStaticData");
const ECDSA = artifacts.require("ECDSA");
const SignatureVerifier = artifacts.require("SignatureVerifier");
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXProgramStoreFactory = artifacts.require("SGXProgramStoreFactory");
const SGXStaticDataMarketPlaceFactory = artifacts.require("SGXStaticDataMarketPlaceFactory");
const SGXKeyVerifier = artifacts.require("SGXKeyVerifier");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const SGXProgramStore = artifacts.require("SGXProgramStore");
const SGXStaticDataMarketPlace = artifacts.require("SGXStaticDataMarketPlace");
const SGXOnChainResultMarket = artifacts.require("SGXOnChainResultMarket")
const SGXOnChainResultMarketImplV1 = artifacts.require("SGXOnChainResultMarketImplV1")
const SGXDataMarketCommon = artifacts.require("SGXDataMarketCommon");
const SGXDataMarketCommonImplV1 = artifacts.require("SGXDataMarketCommonImplV1")
const NaiveOwner = artifacts.require("NaiveOwner");
const IAccessControl = artifacts.require("IAccessControl");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const TokenBankV2 = artifacts.require("TokenBankV2");

const {DHelper, StepRecorder, SoEVerifier} = require("../util.js");

async function performMigration(deployer, network, accounts, dhelper) {

  common = StepRecorder(network, "common")
  sr =  StepRecorder(network, "market")
  or = StepRecorder(network, "owner")

  owner_proxy = or.read("owner-proxy")
  admin = accounts[0]
  if(network.includes("main")){
    admin = "0x5021cBbb7cCaA0B9EC7689164a079e3327E29bEE"
  }

  if(network.includes("main")){
    usdc = "0xA0b86991c6218b36c1d19D4a2e9Eb0cE3606eB48"
    bank = await dhelper.readOrCreateContract(TokenBankV2, [], "TreeHole Asset Pool")
    await bank.transferOwnership(admin)
    fee_pool= bank.address
  }else if(network.includes("ropsten")){
    usdc = "0xd6e66C95Cf41Bec2477E600D33C02982caBd2Ae3"
    fee_pool = accounts[0]
  }else{
    usdc = await dhelper.readOrCreateContract(ERC20Token, [AddressArray], '0x0000000000000000000000000000000000000000', '0x0000000000000000000000000000000000000000', 0, "USDC", 6, "USDC", true);
    tlist_factory = await TrustListFactory.at(common.read("trustlist-factory"))
    tx = await tlist_factory.createTrustList([accounts[0]]);
    tlist = await TrustList.at(tx.logs[0].args.addr);
    await usdc.changeTrustList(tlist.address)
    await usdc.generateTokens(accounts[0], "10000000000000000000000")
    usdc = usdc.address

    fee_pool = accounts[0]
  }

  pstore = await dhelper.readOrCreateContract(SGXProgramStore, [], owner_proxy)
  sr.write("program-store", pstore.address)
  sr.write("owner_proxy", owner_proxy)
  sr.write("payment token", usdc)

  tlist_factory = await TrustListFactory.at(common.read("trustlist-factory"))
  tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
  tlist = await TrustList.at(tx.logs[0].args.addr);
  sr.write('market-tlist', tlist.address)

  market=await dhelper.readOrCreateContract(SGXStaticDataMarketPlace,[SafeMath,SGXStaticData],pstore.address,owner_proxy, usdc)
  await market.changeTrustList(tlist.address)
  sr.write("market", market.address)
  await market.changeFee('100000')
  await market.changeFeePool(fee_pool)
  await market.transferOwnership(admin)



  onchain_impl = await dhelper.readOrCreateContract(SGXOnChainResultMarketImplV1, [SafeMath, SGXStaticData])
  sr.write('onchain-market-impl', onchain_impl.address)
  onchain_market = await dhelper.readOrCreateContract(SGXOnChainResultMarket, [])
  sr.write('onchain-market', onchain_market.address)
  await onchain_market.changeDataLib(onchain_impl.address)
  await onchain_market.changeMarket(market.address)
  await tlist.add_trusted(onchain_market.address)

  common_impl = await dhelper.readOrCreateContract(SGXDataMarketCommonImplV1, [SGXStaticData])
  common_market = await dhelper.readOrCreateContract(SGXDataMarketCommon, [])
  sr.write('common-market', common_market.address)
  sr.write('common-market-impl', common_impl.address)

  await common_market.changeDataLib(common_impl.address)
  await common_market.changeMarket(market.address)
  await tlist.add_trusted(common_market.address)
  ac = await IAccessControl.at(owner_proxy)
  let r = web3.utils.keccak256("INITIALIZER_ROLE")
  await ac.grantRole(r, common_market.address)
  await ac.grantRole(r, pstore.address)

  await onchain_market.transferOwnership(admin)
  await common_market.transferOwnership(admin)
  await tlist.transferOwnership(admin)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
