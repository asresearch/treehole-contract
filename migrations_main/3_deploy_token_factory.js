const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const AddressArray = artifacts.require("AddressArray");
const {DHelper, StepRecorder} = require("../util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  console.log("dhelper", dhelper)
  f = await dhelper.readOrCreateContract(ERC20TokenFactory, [AddressArray]);
  common =  StepRecorder(network, "common")
  common.write("erc20-token-factory", f.address)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
