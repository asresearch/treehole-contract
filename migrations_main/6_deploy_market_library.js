const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const MerkleProof = artifacts.require("MerkleProof");
const SGXRequest = artifacts.require("SGXRequest");
const SGXOnChainResult = artifacts.require("SGXOnChainResult");
const SGXOffChainResult = artifacts.require("SGXOffChainResult");
const SGXStaticData = artifacts.require("SGXStaticData");
const ECDSA = artifacts.require("ECDSA");
const SignatureVerifier = artifacts.require("SignatureVerifier");

const {DHelper} = require("../util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  //await dhelper.readOrCreateContract(SignatureVerifier, []);
  await dhelper.readOrCreateContract(SGXRequest, [SafeMath]);
  await dhelper.readOrCreateContract(SGXOnChainResult, [SafeMath])
  await dhelper.readOrCreateContract(SGXOffChainResult, [SafeMath])
  await dhelper.readOrCreateContract(SGXStaticData, [SGXRequest, SGXOnChainResult, SGXOffChainResult, SafeMath])
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
