const {DHelper, StepRecorder} = require("../util.js");
const TrustListFactory = artifacts.require("TrustListFactory");
const AddressArray = artifacts.require("AddressArray");

async function performMigration(deployer, network, accounts, dhelper) {
  f = await dhelper.readOrCreateContract(TrustListFactory, [AddressArray])
  common =  StepRecorder(network, "common")
  common.write("trustlist-factory", f.address)
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
