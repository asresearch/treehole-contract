const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const MerkleProof = artifacts.require("MerkleProof");
const SGXRequest = artifacts.require("SGXRequest");
const SGXOnChainResult = artifacts.require("SGXOnChainResult");
const SGXStaticData = artifacts.require("SGXStaticData");
const ECDSA = artifacts.require("ECDSA");
const SignatureVerifier = artifacts.require("SignatureVerifier");
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXProgramStoreFactory = artifacts.require("SGXProgramStoreFactory");
const SGXStaticDataMarketPlaceFactory = artifacts.require("SGXStaticDataMarketPlaceFactory");
const SGXKeyVerifier = artifacts.require("SGXKeyVerifier");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const SGXProgramStore = artifacts.require("SGXProgramStore");
const SGXStaticDataMarketPlace = artifacts.require("SGXStaticDataMarketPlace");
const SGXOnChainResultMarket = artifacts.require("SGXOnChainResultMarket")
const SGXOnChainResultMarketImplV1 = artifacts.require("SGXOnChainResultMarketImplV1")
const SGXDataMarketCommon = artifacts.require("SGXDataMarketCommon");
const SGXDataMarketCommonImplV1 = artifacts.require("SGXDataMarketCommonImplV1")
const SGXDataMarketPrice = artifacts.require("SGXDataMarketPrice")
const SGXDataMarketPriceImplV1 = artifacts.require("SGXDataMarketPriceImplV1")

const {DHelper, StepRecorder, SoEVerifier} = require("../util.js");

async function performMigration(deployer, network, accounts, dhelper) {

  sr =  StepRecorder(network, "market")
  tlist = await TrustList.at(sr.read('market-tlist'))
  market = sr.read("market")


  onchain_impl = await dhelper.readOrCreateContract(SGXDataMarketPriceImplV1, [SafeMath, ECDSA])
  sr.write('market-price-impl', onchain_impl.address)
  onchain_market = await dhelper.readOrCreateContract(SGXDataMarketPrice, [])
  sr.write('onchain-price', onchain_market.address)
  await onchain_market.changeDataLib(onchain_impl.address)
  console.log('changeDataLib')
  await onchain_market.changeMarket(market)
  console.log('changeMarket')
  await tlist.add_trusted(onchain_market.address)
  console.log('add_trusted')

}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
