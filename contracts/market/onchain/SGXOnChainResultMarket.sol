pragma solidity >=0.4.21 <0.6.0;

import "../../../utils/Ownable.sol";
import "../interface/DataMarketPlaceInterface.sol";
import "../../../plugins/GasRewardTool.sol";
import "../../../erc20/IERC20.sol";
import "../../../erc20/SafeERC20.sol";
import "../SGXProxyBase.sol";
import "../../mine/MinerProxy.sol";
import "../IERC20Permit.sol";
import "./SGXOnChainResultMarketBase.sol";

pragma experimental ABIEncoderV2;

contract SGXOnChainResultMarket is
    Ownable,
    GasRewardTool,
    SGXProxyBase,
    MinerProxy,
    SGXOnChainResultMarketBase
{
    using SafeERC20 for IERC20;

    function requestOnChain(
        bytes32 _vhash,
        bytes memory secret,
        bytes memory input,
        bytes memory forward_sig,
        bytes32 program_hash,
        uint256 gas_price,
        bytes memory pkey,
        uint256 amount
    )
        public
        rewardGas
        need_confirm(
            _vhash,
            keccak256(
                abi.encode(
                    address(this),
                    pkey,
                    secret,
                    input,
                    forward_sig,
                    program_hash,
                    gas_price,
                    block.number
                )
            )
        )
        returns (bytes32)
    {
        return
            _requestOnChain(
                _vhash,
                secret,
                input,
                forward_sig,
                program_hash,
                gas_price,
                pkey,
                amount
            );
    }

    function submitOnChainResult(
        bytes32 _vhash,
        bytes32 request_hash,
        uint64 cost,
        bytes memory result,
        bytes memory sig
    ) public rewardGas need_confirm(_vhash, request_hash) returns (bool) {
        return _submitOnChainResult(_vhash, request_hash, cost, result, sig);
    }
}
