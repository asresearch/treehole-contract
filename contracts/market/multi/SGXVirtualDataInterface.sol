pragma solidity >=0.4.21 <0.6.0;

contract SGXVirtualDataInterface{

  function createVirtualDataFromMultiData(bytes32[] calldata _vhashes) external returns(bytes32);

}
