pragma solidity >=0.4.21 <0.6.0;
import "../../../utils/Ownable.sol";
import "./SGXVirtualDataInterface.sol";
import "../interface/DataMarketPlaceInterface.sol";
import "../../../erc20/IERC20.sol";
import "../../../erc20/SafeERC20.sol";

pragma experimental ABIEncoderV2;

contract SGXVirtualDataBase  is Ownable{
  using SafeERC20 for IERC20;

  struct data_source_info{
    bytes32 data_vhash;
    uint256 data_price;
  }

  SGXVirtualDataInterface public virtual_lib_address;
  mapping (bytes32 => data_source_info[]) public data_sources;

  event NewVirtualData(bytes32 indexed vhash, bytes32[] vhashes);

  event ChangeVirtualDataLib(address old_lib, address new_lib);
  function changeVirtualDataLib(address _new_lib) public onlyOwner{
    address old = address(virtual_lib_address);
    virtual_lib_address = SGXVirtualDataInterface(_new_lib);
    emit ChangeVirtualDataLib(old, _new_lib);
  }

  function createVirtualData(DataMarketPlaceInterface market, bytes32[] memory _vhashes) internal returns(bytes32){
    bytes32 vhash;
    {
      //virtual_lib_address.createVirtualDataFromMultiData.selector;
      //bytes memory data;
      bytes memory data = abi.encodeWithSelector(virtual_lib_address.createVirtualDataFromMultiData.selector, _vhashes);
      bytes memory ret = market.delegateCallUseData(address(virtual_lib_address), data);
      (vhash) = abi.decode(ret, (bytes32));
    }
    emit NewVirtualData(vhash, _vhashes);
    return vhash;
  }

  function recordDataSource(DataMarketPlaceInterface market, bytes32 request_hash, bytes32[] memory _vhashes) internal{
    data_source_info[] storage dsi = data_sources[request_hash];
    for(uint i = 0; i < _vhashes.length; i++){
      (,,uint256 price, ,,,, ) = market.getDataInfo(_vhashes[i]);
      data_source_info memory dmi;
      dmi.data_price = price;
      dmi.data_vhash = _vhashes[i];
      dsi.push(dmi);
    }
  }

  function dispatchFee(DataMarketPlaceInterface market, bytes32 request_hash) internal{
    address token = market.payment_token();
    if(token == address(0x0)){
      return ;
    }
    if(IERC20(token).balanceOf(address(this)) == 0){
      return ;
    }

    data_source_info[] storage dsi = data_sources[request_hash];
    for(uint i = 0; i < dsi.length; i++){
      (,,, ,address owner,,, ) = market.getDataInfo(dsi[i].data_vhash);
      IERC20(token).safeTransfer(owner, dsi[i].data_price);
    }
  }
}
