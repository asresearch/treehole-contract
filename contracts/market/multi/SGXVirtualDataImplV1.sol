pragma solidity >=0.4.21 <0.6.0;
import "../onchain/SGXOnChainResult.sol";
import "../interface/ProgramProxyInterface.sol";
import "../interface/KeyVerifierInterface.sol";
import "../SignatureVerifier.sol";
import "../../../erc20/IERC20.sol";
import "../../../erc20/SafeERC20.sol";
import "../../../utils/Address.sol";
import "../SGXRequest.sol";
import "../SGXStaticData.sol";
import "../SGXStaticDataMarketStorage.sol";
import "../common/QuickSort.sol";

contract SGXVirtualDataImplV1 is SGXStaticDataMarketStorage{
  using SafeMath for uint256;
  using SGXRequest for mapping(bytes32 => SGXRequest.Request);
  using SGXStaticData for mapping(bytes32 =>SGXStaticData.Data);

  using SignatureVerifier for bytes32;
  using ECDSA for bytes32;
  using SafeERC20 for IERC20;
  using Address for address;

  constructor() public {}

  function createVirtualDataFromMultiData(bytes32[] memory _vhashes) public returns(bytes32){
    bytes32[] memory hashes = new bytes32[](_vhashes.length);
    for(uint i = 0; i < _vhashes.length; i++){
      hashes[i] = all_data[_vhashes[i]].data_hash;
    }

    bytes32[] memory ts = QuickSort.sort(hashes);

    bytes32 hash = keccak256(abi.encodePacked(ts));
    string memory extra = iToHex(abi.encodePacked(ts));
    uint256 price = 0;
    uint256 revoke_block_num = 0;
    for(uint i = 0; i < _vhashes.length; i++){
      require(all_data[_vhashes[i]].exists, "data vhash not exist");
      price = price + all_data[_vhashes[i]].price;
      if(all_data[_vhashes[i]].revoke_timeout_block_num > revoke_block_num){
        revoke_block_num = all_data[_vhashes[i]].revoke_timeout_block_num;
      }
    }
    bytes32 vhash = all_data.init(hash, extra, price, revoke_block_num, bytes(extra));
    owner_proxy.initOwnerOf(vhash, msg.sender);
    return vhash;
  }
  function iToHex(bytes memory buffer) public pure returns (string memory) {

            // Fixed buffer size for hexadecimal convertion
    bytes memory converted = new bytes(buffer.length * 2);

    bytes memory _base = "0123456789abcdef";

    for (uint256 i = 0; i < buffer.length; i++) {
        converted[i * 2] = _base[uint8(buffer[i]) / _base.length];
        converted[i * 2 + 1] = _base[uint8(buffer[i]) % _base.length];
    }

    return string(abi.encodePacked("0x", converted));
  }
}

