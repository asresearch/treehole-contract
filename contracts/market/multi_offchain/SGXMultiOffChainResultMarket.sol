pragma solidity >=0.4.21 <0.6.0;
import "../../../utils/Ownable.sol";
import "../interface/DataMarketPlaceInterface.sol";
import "../../../plugins/GasRewardTool.sol";
import "../../../erc20/IERC20.sol";
import "../../../erc20/SafeERC20.sol";
import "../SGXProxyBase.sol";
import "../multi/SGXVirtualDataBase.sol";
import "../offchain/SGXOffChainResultMarketBase.sol";
pragma experimental ABIEncoderV2;

contract SGXMultiOffChainResultMarket is
    Ownable,
    GasRewardTool,
    SGXProxyBase,
    SGXVirtualDataBase,
    SGXOffChainResultMarketBase
{
    using SafeERC20 for IERC20;

    function requestOffChain(
        bytes32[] memory _vhashes,
        bytes memory secret,
        bytes memory input,
        bytes memory forward_sig,
        bytes32 program_hash,
        uint256 gas_price,
        bytes memory pkey,
        uint256 amount
    ) public rewardGas returns (bytes32, bytes32) {
        bytes32 vhash = createVirtualData(market, _vhashes);

        // confirm before
        bytes32 transferReqeustHash;
        {
            transferReqeustHash = _beforeConfirm(
                vhash,
                keccak256(
                    abi.encode(
                        address(this),
                        pkey,
                        secret,
                        input,
                        forward_sig,
                        program_hash,
                        gas_price,
                        block.number
                    )
                )
            );
        }
        bytes32 request_hash = _requestOffChain(
            vhash,
            secret,
            input,
            forward_sig,
            program_hash,
            gas_price,
            pkey,
            amount
        );
        // create a copy of _vhashes to aovid stack too deep error
        bytes32[] memory vhashes = _vhashes;
        recordDataSource(market, request_hash, vhashes);

        // confirm after
        _afterConfirm(vhash, request_hash, transferReqeustHash);

        return (vhash, request_hash);
    }

    function submitOffChainSkey(
        bytes32 _vhash,
        bytes32 request_hash,
        uint64 cost,
        bytes memory skey,
        bytes memory sig
    ) public rewardGas need_confirm(_vhash, request_hash) returns (bool) {
        bool v = _submitOffChainSkey(_vhash, request_hash, cost, skey, sig);
        dispatchFee(market, request_hash);
        return v;
    }
}
