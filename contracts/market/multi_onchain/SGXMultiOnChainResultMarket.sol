pragma solidity >=0.4.21 <0.6.0;

import "../../../utils/Ownable.sol";
import "../interface/DataMarketPlaceInterface.sol";
import "../../../plugins/GasRewardTool.sol";
import "../../../erc20/IERC20.sol";
import "../../../erc20/SafeERC20.sol";
import "../SGXProxyBase.sol";
import "../../mine/MinerProxy.sol";
import "../IERC20Permit.sol";
import "../multi/SGXVirtualDataBase.sol";
import "../onchain/SGXOnChainResultMarketBase.sol";

pragma experimental ABIEncoderV2;

contract SGXMultiOnChainResultMarket is
    Ownable,
    GasRewardTool,
    SGXProxyBase,
    MinerProxy,
    SGXVirtualDataBase,
    SGXOnChainResultMarketBase
{
    using SafeERC20 for IERC20;

    struct RequestParam {
        bytes secret;
        bytes input;
        bytes forward_sig;
        bytes32 program_hash;
        bytes pkey;
    }

    function requestOnChain(
        bytes32[] memory _vhashes,
        RequestParam memory param,
        uint256 gas_price,
        uint256 amount
    ) public rewardGas returns (bytes32, bytes32) {
        bytes32 vhash = createVirtualData(market, _vhashes);

        // confirm before
        bytes32 transferRequestHash;
        {
            transferRequestHash = _beforeConfirm(
                vhash,
                keccak256(
                    abi.encode(
                        address(this),
                        param.pkey,
                        param.secret,
                        param.input,
                        param.forward_sig,
                        param.program_hash,
                        gas_price,
                        block.number
                    )
                )
            );
        }

        bytes32 request_hash = _requestOnChain(
            vhash,
            param.secret,
            param.input,
            param.forward_sig,
            param.program_hash,
            gas_price,
            param.pkey,
            amount
        );
        recordDataSource(market, request_hash, _vhashes);

        // confirm after
        _afterConfirm(vhash, request_hash, transferRequestHash);

        return (vhash, request_hash);
    }

    function submitOnChainResult(
        bytes32 _vhash,
        bytes32 request_hash,
        uint64 cost,
        bytes memory result,
        bytes memory sig
    ) public rewardGas returns (bool) {
        // confirm before
        bytes32 transferRequestHash;
        {
            transferRequestHash = _beforeConfirm(
                _vhash,
                request_hash
            );
        }

        bool v = _submitOnChainResult(_vhash, request_hash, cost, result, sig);
        dispatchFee(market, request_hash);

        // confirm after
        _afterConfirm(_vhash, request_hash, transferRequestHash);

        _autoTransferCommit(transferRequestHash, true);
        return v;
    }
}
