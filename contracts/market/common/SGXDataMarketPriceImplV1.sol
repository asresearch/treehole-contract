pragma solidity >=0.4.21 <0.6.0;
import "../interface/DataMarketPlaceInterface.sol";
import "../SGXStaticData.sol";
import "../SGXStaticDataMarketStorage.sol";
import "../SignatureVerifier.sol";
import "../../../erc20/IERC20.sol";
import "../../../erc20/SafeERC20.sol";


contract SGXDataMarketPriceImplV1 is SGXStaticDataMarketStorage{
  using SGXStaticData for mapping(bytes32=>SGXStaticData.Data);
  using SignatureVerifier for bytes32;
  using ECDSA for bytes32;
  using SafeERC20 for IERC20;

  constructor() public{}
  function changeDataPrice(bytes32 _vhash, uint256 new_price) public returns(uint256){
    require(!paused, "already paused to use");
    require(all_data[_vhash].exists, "data vhash not exist");
    uint256 old_price = all_data[_vhash].price;
    all_data[_vhash].price = new_price;
    return old_price;
  }

}
