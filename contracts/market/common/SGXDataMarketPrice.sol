pragma solidity >=0.4.21 <0.6.0;
import "../../../utils/Ownable.sol";
import "../interface/DataMarketPlaceInterface.sol";
import "../../../plugins/GasRewardTool.sol";
import "../../../erc20/IERC20.sol";
import "../../../erc20/SafeERC20.sol";
import "../SGXProxyBase.sol";

interface IMarketCommonPrice{
  function getDataOwner(bytes32 _vhash) external returns(address);
}

contract SGXDataMarketPrice is Ownable, GasRewardTool, SGXProxyBase{

  event SDMarketChangeDataPrice(bytes32 indexed vhash, uint256 old_price, uint256 new_price);
  function changeDataPrice(bytes32 _hash, uint256 new_price) public{

    address owner = getDataOwner(_hash);
    require(owner == msg.sender, "only data owner may change price");

    bytes memory d = abi.encodeWithSignature("changeDataPrice(bytes32,uint256)", _hash, new_price);
    bytes memory ret = market.delegateCallUseData(data_lib_address, d);
    (uint256 old_price) = abi.decode(ret, (uint256));
    emit SDMarketChangeDataPrice(_hash, old_price, new_price);
  }

  function getDataOwner(bytes32 _vhash) public view returns(address){
    return market.owner_proxy().ownerOf(_vhash);
  }
}
