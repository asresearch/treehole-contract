pragma solidity >=0.4.21 <0.6.0;

contract IPeriodAmount{
  function getPeriodAmount() public view returns(uint256);
}
