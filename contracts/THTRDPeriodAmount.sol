pragma solidity >=0.4.21 <0.6.0;
import "./IPeriodAmount.sol";
import "../utils/Ownable.sol";
import "../utils/SafeMath.sol";

contract THMintInterfaceForPA{
  uint256 public period_amount;
}

contract THTokenRaiseInterfaceForPA{
  function get_share_fraction() public view returns(uint256, uint256);
}

contract THTRDPeriodAmount is IPeriodAmount, Ownable{
  using SafeMath for uint256;
  THTokenRaiseInterfaceForPA public THraise;
  THMintInterfaceForPA public minter;
  uint256 public TRfrac_n;// the fraction of TR
  uint256 public TRfrac_d;

  constructor(uint256 _n, uint256 _d, address _minter, address _raise) public{
    TRfrac_n = _n;
    TRfrac_d = _d;
    minter = THMintInterfaceForPA(_minter);
    THraise = THTokenRaiseInterfaceForPA(_raise);
  }

  function getPeriodAmount() public view returns(uint256){
    (uint256 share_n, uint256 share_d) = THraise.get_share_fraction();
    return minter.period_amount().safeMul(share_n).safeMul(TRfrac_n).safeDiv(share_d).safeDiv(TRfrac_d);
  }
}

contract THTRDPeriodAmountFactory{
  event NewTHTRDPeriodAmount(address addr);
  function createTHTRDPeriodAmount(uint256 _n, uint256 _d, address _minter, address _raise) public returns(address){
    THTRDPeriodAmount pa = new THTRDPeriodAmount(_n, _d, _minter, _raise);
    emit NewTHTRDPeriodAmount(address(pa));
    pa.transferOwnership(msg.sender);
    return address(pa);
  }
}

