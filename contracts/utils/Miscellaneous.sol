// SPDX-License-Identifier: MIT

pragma solidity >=0.4.21 <0.6.0;

import "../../erc20/IERC20.sol";
import "../../erc20/SafeERC20.sol";

library Miscellaneous {
    using SafeMath for uint256;
    using SafeERC20 for IERC20;

    function transferConfirmedAndApprove(
        IERC20 token,
        address to,
        uint256 amount
    ) public {
        uint256 balanceBefore = token.balanceOf(address(this));
        token.safeTransferFrom(msg.sender, address(this), amount);
        uint256 balanceAfter = token.balanceOf(address(this));
        require(balanceAfter.safeSub(balanceBefore) == amount, "invalid amount");

        token.safeApprove(to, 0);
        token.safeApprove(to, amount);
    }
}
