pragma solidity >=0.4.21 <0.6.0;
import "../IPeriodAmount.sol";

contract TRDPeriodAmountMock is IPeriodAmount{

  function getPeriodAmount() public view returns(uint256){
    return 1e18;
  }

}
