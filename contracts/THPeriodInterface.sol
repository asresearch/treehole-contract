pragma solidity >=0.4.21 <0.6.0;
contract THPeriodInterface{
  function current_period() public view returns(uint256);
  function get_current_period() public returns(uint256);
}
