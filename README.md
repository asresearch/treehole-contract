# TreeHole Contract

## Build

TreeHole is based on `ETH-Contracts`, so you have to clone that project first,
and do the build in the `eth-contracts` directory. Here are the complete instructions to setup the project.


```
git clone https://gitlab.com/asresearch/eth-contracts.git
git clone https://gitlab.com/asresearch/treehole-contract.git

cd eth-contracts/contracts
ln -snf ../../treehole-contract/contracts core
cd ../
ln -snf ../treehole-contract/migrations migrations
ln -snf ../treehole-contract/test test
npm install
cd ../treehole-contract
ln -snf ../eth-contracts/node_modules node_modules
```


As long as the setup proecess is done, you may build the project now:

```
cd eth-contracts
truffle compile
```


## Test

You may start `ganache-cli` first and use port 7545, like this
```
ganache-cli --host 127.0.0.1 --port 7545 -i 5777
```

In order to run test cases, follow the instructions:

```
cd eth-contracts
ln -snf truffle-config.ganache.js truffle-config.js
truffle test ./test/test_dispacher.js --network ganache
```
Or you may change `./test/test_dispacher.js` to other file for other testing.


