const {
  BN,
  constants,
  expectEvent,
  expectRevert,
} = require("@openzeppelin/test-helpers");
const { expect } = require("chai");
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXKeyVerifier = artifacts.require("SGXKeyVerifier");
const SGXProgramStoreFactory = artifacts.require("SGXProgramStoreFactory");
const SGXProgramStore = artifacts.require("SGXProgramStore");
const SGXStaticDataMarketPlaceFactory = artifacts.require(
  "SGXStaticDataMarketPlaceFactory"
);
const SGXStaticDataMarketPlace = artifacts.require("SGXStaticDataMarketPlace");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const TokenBankV2 = artifacts.require("TokenBankV2");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const USDT = artifacts.require("USDT");
const SGXMultiOffChainResultMarket = artifacts.require(
  "SGXMultiOffChainResultMarket"
);
const { StepRecorder } = require("./util.js");
const SGXOffChainResultMarketImplV1 = artifacts.require(
  "SGXOffChainDataMarketImplV1"
);
const SGXDataMarketCommon = artifacts.require("SGXDataMarketCommon");
const SGXRequest = artifacts.require("SGXRequest");

const fs = require("fs");
const data = JSON.parse(
  fs.readFileSync("./test/multi_offchain_summary.json", "utf-8").toString()
);

contract("Test Multi Offchain Market", (accounts) => {
  let program_store = {};
  // let verifier = {};
  let market = {};
  let token = {};
  let pkey = {};
  let program_hash = {};
  let data_vhash = {};
  let request_hash = {};
  let offchain_market = {};
  let common_market = {};

  //upload program and data
  let enclave_hash = "0x" + data.enclave_hash;

  let data_hash1 = "0x" + data.input[0].input_data_hash;
  let pkey_data1 = "0x" + data.input[0]["public-key"];
  let hash_sig1 = "0x" + data.input[0].hash_sig;

  let data_hash2 = "0x" + data.input[1].input_data_hash;
  let pkey_data2 = "0x" + data.input[1]["public-key"];
  let hash_sig2 = "0x" + data.input[1].hash_sig;

  //requestOffChain
  let secret = "0x" + data.request_info.shu_info.encrypted_skey;
  let input = "0x" + data["analyzer-input"];
  let forward_sig = "0x" + data.request_info.shu_info.forward_sig;
  let pkey_request = "0x" + data["request_info"]["analyzer-pkey"];

  //submitOffChainResultReady && remindCost
  let sig_ready = "0x" + data["cost-signature"];

  // requestOffChainSkey
  let result_hash = "0x" + data.result_hash;
  //"0xc18d89f0ec84aacb4eb017c61e022d53d88e3ee6708503e4cf7acb96a03c9f37";

  // submitOffChainSkey
  let skey = "0x" + data.result_encrypt_key;
  let sig_submit = "0x" + data["result-signature"];

  let data_vhash1;
  let data_vhash2;

  context("Test basic function", async () => {
    it("init", async () => {
      sr = StepRecorder("ganache", "market");
      token = await ERC20Token.at(sr.value("token"));
      l = await token.balanceOf(accounts[4]);
      await token.destroyTokens(accounts[4], l);
      l = await token.balanceOf(accounts[3]);
      await token.destroyTokens(accounts[3], l);
      l = await token.balanceOf(accounts[2]);
      await token.destroyTokens(accounts[2], l);
      l = await token.balanceOf(accounts[1]);
      await token.destroyTokens(accounts[1], l);
      l = await token.balanceOf(accounts[0]);
      await token.destroyTokens(accounts[0], l);
      await token.generateTokens(accounts[3], 10 ** 6);
      await token.generateTokens(accounts[4], 10 ** 6);
      market = await SGXStaticDataMarketPlace.at(sr.value("market"));
      offchain_market = await SGXMultiOffChainResultMarket.at(
        sr.value("multi-offchain-market")
      );
      // verifier = await SGXKeyVerifier.at(sr.value("verifier"));
      program_store = await SGXProgramStore.at(sr.value("program-store"));
      common_market = await SGXDataMarketCommon.at(sr.value("common-market"));
      // await verifier.set_verifier_addr("0xf4267391072B27D76Ed8f2A9655BCf5246013F2d", true);
    });

    it("upload program", async () => {
      tx = await program_store.upload_program("test_url", 500, enclave_hash, {
        from: accounts[0],
      });
      program_hash = tx.logs[0].args.hash;
    });

    it("upload data", async () => {
      //we use program_hash as format_lib_hash, it's a mock
      tx = await common_market.createStaticData(
        data_hash1,
        "test env",
        5000,
        pkey_data1,
        hash_sig1,
        { from: accounts[1] }
      );
      data_vhash1 = tx.logs[0].args.vhash;

      tx = await common_market.createStaticData(
        data_hash2,
        "test env",
        5000,
        pkey_data2,
        hash_sig2,
        { from: accounts[2] }
      );
      data_vhash2 = tx.logs[0].args.vhash;

      data_vhash = [data_vhash1, data_vhash2];
    });

    it("request offchain", async () => {
      let gas_price = 10 ** 10;
      let amount = 10 ** 3;
      await token.approve(offchain_market.address, 10 ** 15, {
        from: accounts[3],
      });
      tx = await offchain_market.requestOffChain(
        data_vhash,
        secret,
        input,
        forward_sig,
        program_hash,
        gas_price,
        pkey_request,
        amount,
        { from: accounts[3] }
      );

      // console.log(tx.logs[0].args);
      // console.log(tx.logs[1].args);

      data_vhash = tx.logs[0].args.vhash;
      request_hash = tx.logs[1].args.request_hash;
      // console.log(
      //   "data_vhash: ",
      //   data_vhash,
      //   " --> ",
      //   await market.all_data(data_vhash)
      // );
      // s = await market.getRequestInfo1(data_vhash, request_hash);
      // console.log("Request info: ", s);
      // s = await market.getRequestInfo2(data_vhash, request_hash);
      // console.log("Request info: ", s);
    });

    let gap = {};
    it("remind cost", async () => {
      tx = await offchain_market.remindRequestCost(
        data_vhash,
        request_hash,
        0,
        sig_ready
      );
      gap = tx.logs[0].args.gap;
      console.log("gap: ", gap.toString());
    });

    it("result ready", async () => {
      await offchain_market.submitOffChainResultReady(
        data_vhash,
        request_hash,
        0,
        sig_ready,
        { from: accounts[3] }
      );
    });

    it("request Skey", async () => {
      await offchain_market.requestOffChainSkey(
        data_vhash,
        request_hash,
        result_hash,
        { from: accounts[3] }
      );
    });

    it("Submit Skey - insufficient amount", async () => {
      await expectRevert(
        offchain_market.submitOffChainSkey(
          data_vhash,
          request_hash,
          0,
          skey,
          sig_submit,
          { from: accounts[3] }
        ),
        "insufficient amount"
      );
    });

    it("refund request by another account ", async () => {
      console.log("refund payment: ", gap.toString());
      await token.approve(offchain_market.address, 10 ** 15, {
        from: accounts[4],
      });
      tx = await offchain_market.refundRequest(data_vhash, request_hash, gap, {
        from: accounts[4],
      });
      s = await market.getRequestInfo1(data_vhash, request_hash);
      console.log("Request info: ", s);
    });

    it("Submit Skey again - sufficient amount", async () => {
      tx = await market.getDataInfo(data_vhash1);
      console.log(tx);
      tx = await market.getDataInfo(data_vhash2);
      console.log(tx);
      await offchain_market.submitOffChainSkey(
        data_vhash,
        request_hash,
        0,
        skey,
        sig_submit,
        { from: accounts[3] }
      );
    });

    it("Check each acounts' balance", async () => {
      await expect((await token.balanceOf(accounts[0])).toString()).to.equal(
        "500"
      );
      await expect((await token.balanceOf(accounts[1])).toString()).to.equal(
        "5000"
      );
      await expect((await token.balanceOf(accounts[2])).toString()).to.equal(
        "5000"
      );
      await expect((await token.balanceOf(accounts[3])).toString()).to.equal(
        "999000"
      );
      await expect((await token.balanceOf(accounts[4])).toString()).to.equal(
        "990500"
      );
      await expect((await token.balanceOf(market.address)).toString()).to.equal(
        "0"
      );
    });
  });

  context("Test with payment token being 0x0", async () => {
    it("init", async () => {
      sr = StepRecorder("ganache", "market");
      token = constants.ZERO_ADDRESS;
      // l = await token.balanceOf(accounts[4]);
      // await token.destroyTokens(accounts[4], l);
      // l = await token.balanceOf(accounts[3]);
      // await token.destroyTokens(accounts[3], l);
      // l = await token.balanceOf(accounts[2]);
      // await token.destroyTokens(accounts[2], l);
      // l = await token.balanceOf(accounts[1]);
      // await token.destroyTokens(accounts[1], l);
      // l = await token.balanceOf(accounts[0]);
      // await token.destroyTokens(accounts[0], l);
      // await token.generateTokens(accounts[3], 10 ** 6);
      // await token.generateTokens(accounts[4], 10 ** 6);
      market = await SGXStaticDataMarketPlace.at(sr.value("market-no-payment"));
      offchain_market = await SGXMultiOffChainResultMarket.at(
        sr.value("multi-offchain-market")
      );
      await offchain_market.changeMarket(market.address);
      // verifier = await SGXKeyVerifier.at(sr.value("verifier"));
      program_store = await SGXProgramStore.at(sr.value("program-store"));
      common_market = await SGXDataMarketCommon.at(sr.value("common-market"));
      await common_market.changeMarket(market.address);
      // await verifier.set_verifier_addr("0xf4267391072B27D76Ed8f2A9655BCf5246013F2d", true);
    });

    it("upload program", async () => {
      tx = await program_store.upload_program("test_url", 500, enclave_hash, {
        from: accounts[0],
      });
      program_hash = tx.logs[0].args.hash;
    });

    it("upload data", async () => {
      //we use program_hash as format_lib_hash, it's a mock
      tx = await common_market.createStaticData(
        data_hash1,
        "test env",
        5000,
        pkey_data1,
        hash_sig1,
        { from: accounts[1] }
      );
      data_vhash1 = tx.logs[0].args.vhash;

      tx = await common_market.createStaticData(
        data_hash2,
        "test env",
        5000,
        pkey_data2,
        hash_sig2,
        { from: accounts[2] }
      );
      data_vhash2 = tx.logs[0].args.vhash;

      data_vhash = [data_vhash1, data_vhash2];
    });

    it("request offchain", async () => {
      let gas_price = 10 ** 10;
      let amount = 10 ** 3;
      console.log("payment token: ", await market.payment_token());
      // await token.approve(offchain_market.address, 10 ** 15, {
      //   from: accounts[3],
      // });
      tx = await offchain_market.requestOffChain(
        data_vhash,
        secret,
        input,
        forward_sig,
        program_hash,
        gas_price,
        pkey_request,
        amount,
        { from: accounts[3] }
      );

      // console.log(tx.logs[0].args);
      // console.log(tx.logs[1].args);

      data_vhash = tx.logs[0].args.vhash;
      request_hash = tx.logs[1].args.request_hash;
      // console.log(
      //   "data_vhash: ",
      //   data_vhash,
      //   " --> ",
      //   await market.all_data(data_vhash)
      // );
      // s = await market.getRequestInfo1(data_vhash, request_hash);
      // console.log("Request info: ", s);
      // s = await market.getRequestInfo2(data_vhash, request_hash);
      // console.log("Request info: ", s);
    });

    let gap = {};
    it("remind cost", async () => {
      tx = await offchain_market.remindRequestCost(
        data_vhash,
        request_hash,
        0,
        sig_ready
      );
      gap = tx.logs[0].args.gap;
      console.log("gap: ", gap.toString());
    });

    it("result ready", async () => {
      await offchain_market.submitOffChainResultReady(
        data_vhash,
        request_hash,
        0,
        sig_ready,
        { from: accounts[3] }
      );
    });

    it("request Skey", async () => {
      await offchain_market.requestOffChainSkey(
        data_vhash,
        request_hash,
        result_hash,
        { from: accounts[3] }
      );
    });

    // it("Submit Skey - insufficient amount", async () => {
    //   await expectRevert(
    //     offchain_market.submitOffChainSkey(
    //       data_vhash,
    //       request_hash,
    //       0,
    //       skey,
    //       sig_submit,
    //       { from: accounts[3] }
    //     ),
    //     "insufficient amount"
    //   );
    // });

    it("refund request by another account ", async () => {
      console.log("refund payment: ", gap.toString());
      // await token.approve(offchain_market.address, 10 ** 15, {
      //   from: accounts[4],
      // });
      tx = await offchain_market.refundRequest(data_vhash, request_hash, gap, {
        from: accounts[4],
      });
      s = await market.getRequestInfo1(data_vhash, request_hash);
      console.log("Request info: ", s);
    });

    it("Submit Skey again - sufficient amount", async () => {
      tx = await market.getDataInfo(data_vhash1);
      console.log(tx);
      tx = await market.getDataInfo(data_vhash2);
      console.log(tx);
      await offchain_market.submitOffChainSkey(
        data_vhash,
        request_hash,
        0,
        skey,
        sig_submit,
        { from: accounts[3] }
      );
    });

    // it("Check each acounts' balance", async () => {
    //   await expect((await token.balanceOf(accounts[0])).toString()).to.equal(
    //     "500"
    //   );
    //   await expect((await token.balanceOf(accounts[1])).toString()).to.equal(
    //     "5000"
    //   );
    //   await expect((await token.balanceOf(accounts[2])).toString()).to.equal(
    //     "5000"
    //   );
    //   await expect((await token.balanceOf(accounts[3])).toString()).to.equal(
    //     "999000"
    //   );
    //   await expect((await token.balanceOf(accounts[4])).toString()).to.equal(
    //     "990500"
    //   );
    //   await expect((await token.balanceOf(market.address)).toString()).to.equal(
    //     "0"
    //   );
    // });
  });

  context("Test revoke", async () => {
    // //upload program and data
    // let enclave_hash = "";

    // let data_hash1 = "";
    // let pkey_data1 = "";
    // let hash_sig1 = "";

    // let data_hash2 = "";
    // let pkey_data2 = "";
    // let hash_sig2 = "";

    // //requestOffChain
    // let secret = "";
    // let input = "";
    // let forward_sig = "";
    // let pkey_request = "";

    // //submitOffChainResultReady && remindCost
    // let sig_ready = "";

    // // requestOffChainSkey
    // let result_hash = "";

    // // submitOffChainSkey
    // let skey = "";
    // let sig_submit = "";

    it("init", async () => {
      sr = StepRecorder("ganache", "market");
      token = await ERC20Token.at(sr.value("token"));
      //destroy previously generated token
      l = await token.balanceOf(accounts[4]);
      await token.destroyTokens(accounts[4], l);
      l = await token.balanceOf(accounts[3]);
      await token.destroyTokens(accounts[3], l);
      l = await token.balanceOf(accounts[2]);
      await token.destroyTokens(accounts[2], l);
      l = await token.balanceOf(accounts[1]);
      await token.destroyTokens(accounts[1], l);
      l = await token.balanceOf(accounts[0]);
      await token.destroyTokens(accounts[0], l);

      await token.generateTokens(accounts[3], 10 ** 6);

      market = await SGXStaticDataMarketPlace.at(sr.value("market"));
      offchain_market = await SGXMultiOffChainResultMarket.at(
        sr.value("multi-offchain-market")
      );
      //   verifier = await SGXKeyVerifier.at(sr.value("verifier"));
      program_store = await SGXProgramStore.at(sr.value("program-store"));
      common_market = await SGXDataMarketCommon.at(sr.value("common-market"));
      //   await verifier.set_verifier_addr("0xf4267391072B27D76Ed8f2A9655BCf5246013F2d",true);
    });

    it("upload program", async () => {
      tx = await program_store.upload_program("test_url", 500, enclave_hash, {
        from: accounts[0],
      });
      program_hash = tx.logs[0].args.hash;
    });

    it("upload data", async () => {
      //we use program_hash as format_lib_hash, it's a mock
      tx = await common_market.createStaticData(
        data_hash1,
        "test env",
        5000,
        pkey_data1,
        hash_sig1,
        { from: accounts[1] }
      );
      data_vhash1 = tx.logs[0].args.vhash;

      tx = await common_market.createStaticData(
        data_hash2,
        "test env",
        5000,
        pkey_data2,
        hash_sig2,
        { from: accounts[2] }
      );
      data_vhash2 = tx.logs[0].args.vhash;

      data_vhash = [data_vhash1, data_vhash2];
    });

    it("request offchain", async () => {
      let gas_price = 10 ** 10;
      let amount = 10 ** 3;
      await token.approve(offchain_market.address, 0, {
        from: accounts[3],
      });
      await token.approve(offchain_market.address, 10 ** 15, {
        from: accounts[3],
      });
      tx = await offchain_market.requestOffChain(
        data_vhash,
        secret,
        input,
        forward_sig,
        program_hash,
        gas_price,
        pkey_request,
        amount,
        { from: accounts[3] }
      );

      data_vhash = tx.logs[0].args.vhash;
      request_hash = tx.logs[1].args.request_hash;
    });

    let gap = {};
    it("remind cost", async () => {
      tx = await offchain_market.remindRequestCost(
        data_vhash,
        request_hash,
        0,
        sig_ready
      );
      gap = tx.logs[0].args.gap;
      // console.log("gap: ", gap.toString());
    });

    it("refund request ", async () => {
      // console.log("refund payment: ", gap.toString());
      tx = await offchain_market.refundRequest(data_vhash, request_hash, gap, {
        from: accounts[3],
      });
      s = await market.getRequestInfo1(data_vhash, request_hash);
      // console.log("Request info: ", s);
    });

    it("revoke by another account", async () => {
      await expectRevert(
        offchain_market.revokeRequest(data_vhash, request_hash, {
          from: accounts[2],
        }),
        "only request owner can revoke"
      );
    });

    it("revoke", async () => {
      await offchain_market.revokeRequest(data_vhash, request_hash, {
        from: accounts[3],
      });
    });

    it("Submit Skey again - sufficient amount", async () => {
      await expectRevert(
        offchain_market.submitOffChainSkey(
          data_vhash,
          request_hash,
          0,
          skey,
          sig_submit,
          { from: accounts[3] }
        ),
        "invalid status"
      );
    });

    it("Check each acounts' balance", async () => {
      await expect((await token.balanceOf(accounts[0])).toString()).to.equal(
        "0"
      );
      await expect((await token.balanceOf(accounts[1])).toString()).to.equal(
        "0"
      );
      await expect((await token.balanceOf(accounts[2])).toString()).to.equal(
        "0"
      );
      await expect((await token.balanceOf(accounts[3])).toString()).to.equal(
        "1000000"
      );
      await expect((await token.balanceOf(market.address)).toString()).to.equal(
        "0"
      );
    });
  });

  context("Test  with cost not being 0", async () => {
    // //upload program and data
    // let enclave_hash = "";

    // let data_hash1 = "";
    // let pkey_data1 = "";
    // let hash_sig1 = "";

    // let data_hash2 = "";
    // let pkey_data2 = "";
    // let hash_sig2 = "";

    // //requestOffChain
    // let secret = "";
    // let input = "";
    // let forward_sig = "";
    // let pkey_request = "";

    // //submitOffChainResultReady && remindCost
    // let sig_ready = "";

    // // requestOffChainSkey
    // let result_hash = "";

    // // submitOffChainSkey
    // let skey = "";
    // let sig_submit = "";

    // let data_vhash;
    // let request_hash;

    let cost = 0;

    it("init", async () => {
      sr = StepRecorder("ganache", "market");
      token = await ERC20Token.at(sr.value("token"));
      //destory previously generated tokens
      l = await token.balanceOf(accounts[3]);
      await token.destroyTokens(accounts[3], l);
      l = await token.balanceOf(accounts[2]);
      await token.destroyTokens(accounts[2], l);
      l = await token.balanceOf(accounts[1]);
      await token.destroyTokens(accounts[1], l);
      l = await token.balanceOf(accounts[0]);
      await token.destroyTokens(accounts[0], l);

      await token.generateTokens(accounts[3], 20000);

      market = await SGXStaticDataMarketPlace.at(sr.value("market"));
      offchain_market = await SGXMultiOffChainResultMarket.at(
        sr.value("multi-offchain-market")
      );
      await offchain_market.changeMarket(market.address);
      //   verifier = await SGXKeyVerifier.at(sr.value("verifier"));
      program_store = await SGXProgramStore.at(sr.value("program-store"));
      common_market = await SGXDataMarketCommon.at(sr.value("common-market"));
      await common_market.changeMarket(market.address);
      //   await verifier.set_verifier_addr("0xf4267391072B27D76Ed8f2A9655BCf5246013F2d",true);
    });

    it("upload program", async () => {
      tx = await program_store.upload_program("test_url", 500, enclave_hash, {
        from: accounts[0],
      });
      program_hash = tx.logs[0].args.hash;
    });

    it("upload data", async () => {
      //we use program_hash as format_lib_hash, it's a mock
      tx = await common_market.createStaticData(
        data_hash1,
        "test env",
        5000,
        pkey_data1,
        hash_sig1,
        { from: accounts[1] }
      );
      data_vhash1 = tx.logs[0].args.vhash;

      tx = await common_market.createStaticData(
        data_hash2,
        "test env",
        5000,
        pkey_data2,
        hash_sig2,
        { from: accounts[2] }
      );
      data_vhash2 = tx.logs[0].args.vhash;

      data_vhash = [data_vhash1, data_vhash2];
    });

    it("request offchain", async () => {
      let gas_price = 1;
      let amount = 1000;
      await token.approve(offchain_market.address, 0, {
        from: accounts[3],
      });
      await token.approve(offchain_market.address, 20000, {
        from: accounts[3],
      });
      tx = await offchain_market.requestOffChain(
        data_vhash,
        secret,
        input,
        forward_sig,
        program_hash,
        gas_price,
        pkey_request,
        amount,
        { from: accounts[3] }
      );

      data_vhash = tx.logs[0].args.vhash;
      request_hash = tx.logs[1].args.request_hash;
      //   console.log(
      //     "data_vhash: ",
      //     data_vhash,
      //     " --> ",
      //     await market.all_data(data_vhash)
      //   );
      s = await market.getRequestInfo2(data_vhash, request_hash);
      //   console.log("Request info: ", s);
    });

    let gap = {};
    it("remind cost", async () => {
      tx = await offchain_market.remindRequestCost(
        data_vhash,
        request_hash,
        cost,
        sig_ready
      );
      gap = tx.logs[0].args.gap;
      //   console.log("gap: ", gap.toString());
    });

    it("result ready", async () => {
      await offchain_market.submitOffChainResultReady(
        data_vhash,
        request_hash,
        cost,
        sig_ready,
        { from: accounts[3] }
      );
    });

    it("request Skey", async () => {
      await offchain_market.requestOffChainSkey(
        data_vhash,
        request_hash,
        result_hash,
        { from: accounts[3] }
      );
    });

    it("Submit Skey - insufficient amount", async () => {
      await expectRevert(
        offchain_market.submitOffChainSkey(
          data_vhash,
          request_hash,
          cost,
          skey,
          sig_submit,
          { from: accounts[3] }
        ),
        "insufficient amount"
      );
    });

    it("refund request", async () => {
      // console.log("refund payment: ", gap.toString());
      tx = await offchain_market.refundRequest(data_vhash, request_hash, gap, {
        from: accounts[3],
      });
    });

    it("Submit Skey again - sufficient amount", async () => {
      await offchain_market.submitOffChainSkey(
        data_vhash,
        request_hash,
        cost,
        skey,
        sig_submit,
        { from: accounts[3] }
      );
    });
    it("Check each acounts' balance", async () => {
      await expect((await token.balanceOf(accounts[0])).toString()).to.equal(
        "500"
      );
      await expect((await token.balanceOf(accounts[1])).toString()).to.equal(
        "5000"
      );
      await expect((await token.balanceOf(accounts[2])).toString()).to.equal(
        "5000"
      );
      await expect((await token.balanceOf(accounts[3])).toString()).to.equal(
        "9500"
      );
      await expect((await token.balanceOf(market.address)).toString()).to.equal(
        "0"
      );
    });
  });
});
