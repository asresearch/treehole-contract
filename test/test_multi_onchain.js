const {
  BN,
  constants,
  expectEvent,
  expectRevert,
} = require("@openzeppelin/test-helpers");
const { expect } = require("chai");
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXKeyVerifier = artifacts.require("SGXKeyVerifier");
const SGXProgramStoreFactory = artifacts.require("SGXProgramStoreFactory");
const SGXProgramStore = artifacts.require("SGXProgramStore");
const SGXStaticDataMarketPlaceFactory = artifacts.require(
  "SGXStaticDataMarketPlaceFactory"
);
const SGXStaticDataMarketPlace = artifacts.require("SGXStaticDataMarketPlace");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const TokenBankV2 = artifacts.require("TokenBankV2");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const USDT = artifacts.require("USDT");
const SGXOnChainResultMarket = artifacts.require("SGXOnChainResultMarket");
const { StepRecorder } = require("./util.js");
const SGXOnChainResultMarketImplV1 = artifacts.require(
  "SGXOnChainResultMarketImplV1"
);
const SGXMultiOnChainResultMarket = artifacts.require(
  "SGXMultiOnChainResultMarket"
);
const SGXDataMarketCommon = artifacts.require("SGXDataMarketCommon");

contract("TESTSGXStaticDataMarket", (accounts) => {
  let factory = {};
  let program_store = {};
  //let verifier = {}
  let market = {};
  let token = {};
  let pkey = {};
  let program_hash = {};
  let data_hash = {};
  let data_vhash = {};
  let request_hash = {};
  let onchain_market = {};
  let common_market = {};

  context("init", async () => {
    it("init", async () => {
      //assert.ok(token);
      sr = StepRecorder("ganache", "market");
      token = await ERC20Token.at(sr.value("token"));
      await token.generateTokens(accounts[0], 1000000000);
      market = await SGXStaticDataMarketPlace.at(sr.value("market"));
      onchain_market = await SGXMultiOnChainResultMarket.at(
        sr.value("multi-onchain-market")
      );
      console.log("onchain market: ", sr.value("onchain-market"));
      //verifier = await SGXKeyVerifier.at(sr.value('verifier'))
      program_store = await SGXProgramStore.at(sr.value("program-store"));
      common_market = await SGXDataMarketCommon.at(sr.value("common-market"));
      //await verifier.set_verifier_addr("0xf4267391072B27D76Ed8f2A9655BCf5246013F2d", true)
    });

    it("upload program", async () => {
      tx = await program_store.upload_program(
        "test_url",
        0,
        "0x80badd1e7f5f749873522cf0f921e4510ba80666d7e7e98375e3f12683641f86"
      );
      program_hash = tx.logs[0].args.hash;
    });

    it("upload data", async () => {
      dhash1 =
        "0xa6f468b0f1c830a7e26ccecb2d5990ad3c27004bf6fc05ea53eda73c83f4cdc2";
      hash_sig1 =
        "0x472e9ad362aea51738a3bffc3e4c6bd47239b8488230ff0429811059a2ac311a20bb944bd94d9bf4668fdf76f1606e79e0c023cbe5b1b923718d1dd921696ac91c";
      pkey1 =
        "0x1dded5db8e469ec0e1c84ed7a1cd1cca17a8bed64c3d37ec7534d6dfcbe2328915f141aefc425cf910253e68e91a1069582d5712486cd0b18c53c8a95a89fb82";

      dhash2 =
        "0x311d09ae35b391f9fb0a3a58d0abf0f886d02f58150067814bbb92a1da642937";
      hash_sig2 =
        "0x9b58c4389dd6b192d701bfac77cc6c13cd21b9ef7730e2e28bedfb4e06a00c987700b71a94e896e7fb461b94e5652d8ca430cd4559a4669c581e149c0bc525411b";
      pkey2 =
        "0x7382a40d02bfe2fd5c21085a8ddd9c4935cfb9c927121f1174e87e72a0d85e0eaaff2e8369aa7bfa749da61f6a75102b673d29bd13b0f3655a0742faddb7f001";
      //we use program_hash as format_lib_hash, it's a mock
      tx = await common_market.createStaticData(
        dhash1,
        "test env",
        0,
        pkey1,
        hash_sig1
      );
      //console.log("tx log: ", tx)
      data_vhash1 = tx.logs[0].args.vhash;
      tx = await common_market.createStaticData(
        dhash2,
        "test env",
        0,
        pkey2,
        hash_sig2
      );
      data_vhash2 = tx.logs[0].args.vhash;
    });

    it("submit request", async () => {
      let secret =
        "0x6a8116af37eaf1d902d0b56c9609812a1cb18cfe706588f03701d1c34df0f6b68ebdc29b04297de4a93fb3340ef726495b5b4e46a288f02f031d6dddf4cbfa24cbe17988a465f0c9f2898c78d7923adc72125ba8854d5ce30b57a780ccd524e318fb4ef9e8e071d695fc565d250dab7f0b25e9d68887f23dfab8a54d";
      let input =
        "0xc6f21b568e8cab53758eabbb1e70ab68fb8259f72af51b5d232d93532c12e74ee661b212a34509a9c30f0b5ac31cc2cc3f41f59e9e1996a9274fe48dcc63009e276ff551823f127b534729b40885f9bed5508ecfaa52817e2d47b29d48119ac42c2f1ff067c2905d126e2c1cdf6afbdecbb1eff50f7559ae7bbd";
      let forward_sig =
        "0x18afb52a3d2ace61679b43f79cbb91916d0db7cdcda861ea4b310d140e163da82c94edbbc8282517894bb3cb35a58c51cb1f071fd0450ea51906918634528d101c";
      let gas_price = 0;

      pkey =
        "0x3081b9c5c5b8eeb666358f476ba3b4a2c637db27e91a8674e8def379fe5e8ec514dd4302e997b35b0705de9a7d781858f0d663ce5189eee2652ea87e289423c0";
      //impl = await SGXOnChainResultMarketImplV1.at(await onchain_market.data_lib_address())
      //var c = new web3.eth.Contract(impl.abi, impl.address)
      //abi = c.methods.requestOnChain(data_vhash, secret, input, forward_sig, program_hash, gas_price, pkey, 0).encodeABI()
      //console.log('data_vhash: ', data_vhash, ' --> ', await market.all_data(data_vhash))
      //console.log('payment token: ', await market.payment_token())

      data_vhash = [data_vhash1, data_vhash2];
      await token.approve(onchain_market.address, 1000000000000000);
      let param = {
        secret: secret,
        input: input,
        forward_sig: forward_sig,
        program_hash: program_hash,
        pkey: pkey,
      };
      tx = await onchain_market.requestOnChain(
        data_vhash,
        param,
        gas_price,
        1000
      );
      //console.log('request log: ', tx.logs)
      data_vhash = tx.logs[0].args.vhash;
      request_hash = tx.logs[1].args.request_hash;
      console.log("data_vhash: ", data_vhash, ", request: ", request_hash);
      t = await market.getDataInfo(data_vhash);
      console.log("t: ", t);
    });

    let gap = {};
    //it('submit cost', async() =>{
    //let cost = 12876
    //let cost_sig = "0x8de4601130a5f12b1f97ad52ebbf9c4cd3aa2ea24c86ea596970e131e0f55e427f9d3c2e83c2dbbfe6c031bdeb58327a53c8525a7ec5b59ba3287e35cd18d5731c"
    //tx = await onchain_market.remindRequestCost(data_vhash, request_hash, cost, cost_sig);
    //gap = tx.logs[0].args.gap
    //console.log('gap: ', gap.toString())
    //})

    it("submit result", async () => {
      let result =
        "0xd5e04c8c23c24b5642dbf5aa4bc91f75a95a78b47d2c8fa69effd48e11c869843f80c63a1357d2c87b1bb8129d3d3b2badd939614a293a18585c1654eb2acc05598230ffe01234d465d00526d9f2bc757d199acf9e5433fbc285181cbe4c413239214a898b6a18376169fda0cfa72ec23675c5574c94b04fbd";
      //let result_signature = '0xa7df753859979484e92d778c4ba1b6c81a7071f91c37f1889c259962d511804869f39994aa50cc49d9c56000eba2531271dfeb968ede4f4248aa2b212d01d1971b';
      let result_signature =
        "0x9caf4c422fde7e9a2278d98bc14e26f4a0f3896b6ce09d5ee0eb1ae97652893d326acf2fbbfc89abe1a014706bc968d508b37321b71da91366333babe9d1b70a1b";
      tx = await onchain_market.submitOnChainResult(
        data_vhash,
        request_hash,
        0,
        result,
        result_signature
      );
    });
  });

  context("Test with payment token being 0x0", async () => {
    it("init", async () => {
      //assert.ok(token);
      sr = StepRecorder("ganache", "market");
      token = constants.ZERO_ADDRESS;
      // await token.generateTokens(accounts[0], 1000000000);
      market = await SGXStaticDataMarketPlace.at(sr.value("market-no-payment"));
      onchain_market = await SGXMultiOnChainResultMarket.at(
        sr.value("multi-onchain-market")
      );
      await onchain_market.changeMarket(market.address);
      console.log("onchain market: ", sr.value("onchain-market"));
      //verifier = await SGXKeyVerifier.at(sr.value('verifier'))
      program_store = await SGXProgramStore.at(sr.value("program-store"));
      common_market = await SGXDataMarketCommon.at(sr.value("common-market"));
      await common_market.changeMarket(market.address);
      //await verifier.set_verifier_addr("0xf4267391072B27D76Ed8f2A9655BCf5246013F2d", true)
    });

    it("upload program", async () => {
      tx = await program_store.upload_program(
        "test_url",
        0,
        "0x80badd1e7f5f749873522cf0f921e4510ba80666d7e7e98375e3f12683641f86"
      );
      program_hash = tx.logs[0].args.hash;
    });

    it("upload data", async () => {
      dhash1 =
        "0xa6f468b0f1c830a7e26ccecb2d5990ad3c27004bf6fc05ea53eda73c83f4cdc2";
      hash_sig1 =
        "0x472e9ad362aea51738a3bffc3e4c6bd47239b8488230ff0429811059a2ac311a20bb944bd94d9bf4668fdf76f1606e79e0c023cbe5b1b923718d1dd921696ac91c";
      pkey1 =
        "0x1dded5db8e469ec0e1c84ed7a1cd1cca17a8bed64c3d37ec7534d6dfcbe2328915f141aefc425cf910253e68e91a1069582d5712486cd0b18c53c8a95a89fb82";

      dhash2 =
        "0x311d09ae35b391f9fb0a3a58d0abf0f886d02f58150067814bbb92a1da642937";
      hash_sig2 =
        "0x9b58c4389dd6b192d701bfac77cc6c13cd21b9ef7730e2e28bedfb4e06a00c987700b71a94e896e7fb461b94e5652d8ca430cd4559a4669c581e149c0bc525411b";
      pkey2 =
        "0x7382a40d02bfe2fd5c21085a8ddd9c4935cfb9c927121f1174e87e72a0d85e0eaaff2e8369aa7bfa749da61f6a75102b673d29bd13b0f3655a0742faddb7f001";
      //we use program_hash as format_lib_hash, it's a mock
      tx = await common_market.createStaticData(
        dhash1,
        "test env",
        0,
        pkey1,
        hash_sig1
      );
      //console.log("tx log: ", tx)
      data_vhash1 = tx.logs[0].args.vhash;
      tx = await common_market.createStaticData(
        dhash2,
        "test env",
        0,
        pkey2,
        hash_sig2
      );
      data_vhash2 = tx.logs[0].args.vhash;
    });

    it("submit request", async () => {
      let secret =
        "0x6a8116af37eaf1d902d0b56c9609812a1cb18cfe706588f03701d1c34df0f6b68ebdc29b04297de4a93fb3340ef726495b5b4e46a288f02f031d6dddf4cbfa24cbe17988a465f0c9f2898c78d7923adc72125ba8854d5ce30b57a780ccd524e318fb4ef9e8e071d695fc565d250dab7f0b25e9d68887f23dfab8a54d";
      let input =
        "0xc6f21b568e8cab53758eabbb1e70ab68fb8259f72af51b5d232d93532c12e74ee661b212a34509a9c30f0b5ac31cc2cc3f41f59e9e1996a9274fe48dcc63009e276ff551823f127b534729b40885f9bed5508ecfaa52817e2d47b29d48119ac42c2f1ff067c2905d126e2c1cdf6afbdecbb1eff50f7559ae7bbd";
      let forward_sig =
        "0x18afb52a3d2ace61679b43f79cbb91916d0db7cdcda861ea4b310d140e163da82c94edbbc8282517894bb3cb35a58c51cb1f071fd0450ea51906918634528d101c";
      let gas_price = 0;

      pkey =
        "0x3081b9c5c5b8eeb666358f476ba3b4a2c637db27e91a8674e8def379fe5e8ec514dd4302e997b35b0705de9a7d781858f0d663ce5189eee2652ea87e289423c0";
      //impl = await SGXOnChainResultMarketImplV1.at(await onchain_market.data_lib_address())
      //var c = new web3.eth.Contract(impl.abi, impl.address)
      //abi = c.methods.requestOnChain(data_vhash, secret, input, forward_sig, program_hash, gas_price, pkey, 0).encodeABI()
      //console.log('data_vhash: ', data_vhash, ' --> ', await market.all_data(data_vhash))
      //console.log('payment token: ', await market.payment_token())

      data_vhash = [data_vhash1, data_vhash2];
      // await token.approve(onchain_market.address, 1000000000000000);
      let param = {
        secret: secret,
        input: input,
        forward_sig: forward_sig,
        program_hash: program_hash,
        pkey: pkey,
      };
      tx = await onchain_market.requestOnChain(
        data_vhash,
        param,
        gas_price,
        1000
      );
      //console.log('request log: ', tx.logs)
      data_vhash = tx.logs[0].args.vhash;
      request_hash = tx.logs[1].args.request_hash;
      console.log("data_vhash: ", data_vhash, ", request: ", request_hash);
      t = await market.getDataInfo(data_vhash);
      console.log("t: ", t);
    });

    let gap = {};
    //it('submit cost', async() =>{
    //let cost = 12876
    //let cost_sig = "0x8de4601130a5f12b1f97ad52ebbf9c4cd3aa2ea24c86ea596970e131e0f55e427f9d3c2e83c2dbbfe6c031bdeb58327a53c8525a7ec5b59ba3287e35cd18d5731c"
    //tx = await onchain_market.remindRequestCost(data_vhash, request_hash, cost, cost_sig);
    //gap = tx.logs[0].args.gap
    //console.log('gap: ', gap.toString())
    //})

    it("submit result", async () => {
      let result =
        "0xd5e04c8c23c24b5642dbf5aa4bc91f75a95a78b47d2c8fa69effd48e11c869843f80c63a1357d2c87b1bb8129d3d3b2badd939614a293a18585c1654eb2acc05598230ffe01234d465d00526d9f2bc757d199acf9e5433fbc285181cbe4c413239214a898b6a18376169fda0cfa72ec23675c5574c94b04fbd";
      //let result_signature = '0xa7df753859979484e92d778c4ba1b6c81a7071f91c37f1889c259962d511804869f39994aa50cc49d9c56000eba2531271dfeb968ede4f4248aa2b212d01d1971b';
      let result_signature =
        "0x9caf4c422fde7e9a2278d98bc14e26f4a0f3896b6ce09d5ee0eb1ae97652893d326acf2fbbfc89abe1a014706bc968d508b37321b71da91366333babe9d1b70a1b";
      tx = await onchain_market.submitOnChainResult(
        data_vhash,
        request_hash,
        0,
        result,
        result_signature
      );
    });
  });

  context("test price not 0", async () => {
    it("init", async () => {
      //assert.ok(token);
      sr = StepRecorder("ganache", "market");
      token = await ERC20Token.at(sr.value("token"));
      await token.generateTokens(accounts[0], 1000000000);
      market = await SGXStaticDataMarketPlace.at(sr.value("market"));
      onchain_market = await SGXMultiOnChainResultMarket.at(
        sr.value("multi-onchain-market")
      );
      console.log("onchain market: ", sr.value("onchain-market"));
      //verifier = await SGXKeyVerifier.at(sr.value('verifier'))
      program_store = await SGXProgramStore.at(sr.value("program-store"));
      common_market = await SGXDataMarketCommon.at(sr.value("common-market"));
      //await verifier.set_verifier_addr("0xf4267391072B27D76Ed8f2A9655BCf5246013F2d", true)
    });

    it("upload program", async () => {
      tx = await program_store.upload_program(
        "test_url",
        500,
        "0x80badd1e7f5f749873522cf0f921e4510ba80666d7e7e98375e3f12683641f86"
      );
      program_hash = tx.logs[0].args.hash;
    });

    it("upload data", async () => {
      dhash1 =
        "0xa6f468b0f1c830a7e26ccecb2d5990ad3c27004bf6fc05ea53eda73c83f4cdc2";
      hash_sig1 =
        "0x472e9ad362aea51738a3bffc3e4c6bd47239b8488230ff0429811059a2ac311a20bb944bd94d9bf4668fdf76f1606e79e0c023cbe5b1b923718d1dd921696ac91c";
      pkey1 =
        "0x1dded5db8e469ec0e1c84ed7a1cd1cca17a8bed64c3d37ec7534d6dfcbe2328915f141aefc425cf910253e68e91a1069582d5712486cd0b18c53c8a95a89fb82";

      dhash2 =
        "0x311d09ae35b391f9fb0a3a58d0abf0f886d02f58150067814bbb92a1da642937";
      hash_sig2 =
        "0x9b58c4389dd6b192d701bfac77cc6c13cd21b9ef7730e2e28bedfb4e06a00c987700b71a94e896e7fb461b94e5652d8ca430cd4559a4669c581e149c0bc525411b";
      pkey2 =
        "0x7382a40d02bfe2fd5c21085a8ddd9c4935cfb9c927121f1174e87e72a0d85e0eaaff2e8369aa7bfa749da61f6a75102b673d29bd13b0f3655a0742faddb7f001";
      //we use program_hash as format_lib_hash, it's a mock
      tx = await common_market.createStaticData(
        dhash1,
        "test env",
        5000,
        pkey1,
        hash_sig1
      );
      //console.log("tx log: ", tx)
      data_vhash1 = tx.logs[0].args.vhash;
      tx = await common_market.createStaticData(
        dhash2,
        "test env",
        5000,
        pkey2,
        hash_sig2
      );
      data_vhash2 = tx.logs[0].args.vhash;
    });

    it("submit request", async () => {
      let secret =
        "0x6a8116af37eaf1d902d0b56c9609812a1cb18cfe706588f03701d1c34df0f6b68ebdc29b04297de4a93fb3340ef726495b5b4e46a288f02f031d6dddf4cbfa24cbe17988a465f0c9f2898c78d7923adc72125ba8854d5ce30b57a780ccd524e318fb4ef9e8e071d695fc565d250dab7f0b25e9d68887f23dfab8a54d";
      let input =
        "0xc6f21b568e8cab53758eabbb1e70ab68fb8259f72af51b5d232d93532c12e74ee661b212a34509a9c30f0b5ac31cc2cc3f41f59e9e1996a9274fe48dcc63009e276ff551823f127b534729b40885f9bed5508ecfaa52817e2d47b29d48119ac42c2f1ff067c2905d126e2c1cdf6afbdecbb1eff50f7559ae7bbd";
      let forward_sig =
        "0x18afb52a3d2ace61679b43f79cbb91916d0db7cdcda861ea4b310d140e163da82c94edbbc8282517894bb3cb35a58c51cb1f071fd0450ea51906918634528d101c";
      let gas_price = 0;

      pkey =
        "0x3081b9c5c5b8eeb666358f476ba3b4a2c637db27e91a8674e8def379fe5e8ec514dd4302e997b35b0705de9a7d781858f0d663ce5189eee2652ea87e289423c0";
      //impl = await SGXOnChainResultMarketImplV1.at(await onchain_market.data_lib_address())
      //var c = new web3.eth.Contract(impl.abi, impl.address)
      //abi = c.methods.requestOnChain(data_vhash, secret, input, forward_sig, program_hash, gas_price, pkey, 0).encodeABI()
      //console.log('data_vhash: ', data_vhash, ' --> ', await market.all_data(data_vhash))
      //console.log('payment token: ', await market.payment_token())

      data_vhash = [data_vhash1, data_vhash2];
      l = await token.balanceOf(accounts[0]);
      await token.destroyTokens(accounts[0], l);
      await token.generateTokens(accounts[0], 1000000000);
      await token.approve(onchain_market.address, 0);
      await token.approve(onchain_market.address, 1000000000000000);
      let param = {
        secret: secret,
        input: input,
        forward_sig: forward_sig,
        program_hash: program_hash,
        pkey: pkey,
      };
      tx = await onchain_market.requestOnChain(
        data_vhash,
        param,
        gas_price,
        1000
      );
      //console.log('request log: ', tx.logs)
      data_vhash = tx.logs[0].args.vhash;
      request_hash = tx.logs[1].args.request_hash;
      console.log("data_vhash: ", data_vhash, ", request: ", request_hash);
      t = await market.getDataInfo(data_vhash);
      console.log("t: ", t);
    });

    let gap = {};
    //it('submit cost', async() =>{
    //let cost = 12876
    //let cost_sig = "0x8de4601130a5f12b1f97ad52ebbf9c4cd3aa2ea24c86ea596970e131e0f55e427f9d3c2e83c2dbbfe6c031bdeb58327a53c8525a7ec5b59ba3287e35cd18d5731c"
    //tx = await onchain_market.remindRequestCost(data_vhash, request_hash, cost, cost_sig);
    //gap = tx.logs[0].args.gap
    //console.log('gap: ', gap.toString())
    //})

    it("submit result", async () => {
      let result =
        "0xd5e04c8c23c24b5642dbf5aa4bc91f75a95a78b47d2c8fa69effd48e11c869843f80c63a1357d2c87b1bb8129d3d3b2badd939614a293a18585c1654eb2acc05598230ffe01234d465d00526d9f2bc757d199acf9e5433fbc285181cbe4c413239214a898b6a18376169fda0cfa72ec23675c5574c94b04fbd";
      //let result_signature = '0xa7df753859979484e92d778c4ba1b6c81a7071f91c37f1889c259962d511804869f39994aa50cc49d9c56000eba2531271dfeb968ede4f4248aa2b212d01d1971b';
      let result_signature =
        "0x9caf4c422fde7e9a2278d98bc14e26f4a0f3896b6ce09d5ee0eb1ae97652893d326acf2fbbfc89abe1a014706bc968d508b37321b71da91366333babe9d1b70a1b";
      tx = await onchain_market.submitOnChainResult(
        data_vhash,
        request_hash,
        0,
        result,
        result_signature
      );
    });
  });
});
