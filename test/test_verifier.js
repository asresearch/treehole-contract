const { BN, constants, expectEvent, expectRevert } = require('@openzeppelin/test-helpers');
const { expect } = require('chai');
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXKeyVerifier = artifacts.require("SGXKeyVerifier");

contract('TESTSGXKeyVerifier', (accounts) => {

  let factory = {}
  let verifier = {}
  let pkey;
  let program_hash = {}

	context('init', async () => {
    it('init', async()=>{
      factory = await SGXKeyVerifierFactory.deployed();
      tx = await factory.createSGXKeyVerifier();
      verifier = await SGXKeyVerifier.at(tx.logs[0].args.addr);
      assert.ok(verifier);
    })

    it('test verify', async() =>{
      await verifier.set_verifier_addr("0xf4267391072B27D76Ed8f2A9655BCf5246013F2d", true)
      let pkey = '0x362a609ab5a6eecafdb2289890bd7261871c04fb5d7323d4fc750f6444b067a12a96efbe24c62572156caa514657d4a535101d2147337f41f51fcdfcf8f43a53'
      let pkey_sig = '0xd9b0a2d2a1c669c7cfd40e1bb71041597140cfff38ec36ff4027405bc18e0b0f2109b354641feda4c4c38bc17836e8b1d15b2054b0c359347595783f9d0664021b'
      ret = await verifier.verify_pkey(pkey, pkey_sig)
      expect(ret).equal(true)
      console.log(ret)
    })
	})


});
