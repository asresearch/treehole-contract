const { expect } = require('chai');
const {DHelper, StepRecorder} = require("./util.js");

const { BN, constants, expectEvent, expectRevert  } = require('@openzeppelin/test-helpers');
const { keccak256, bufferToHex, toBuffer } = require('ethereumjs-util');
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");

const getBlockNumber = require('./blockNumber')(web3)
const TokenBankV2 = artifacts.require("TokenBankV2");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const THTokenRaiseFactory = artifacts.require("THTokenRaiseFactory")
const THTokenRaise = artifacts.require("THTokenRaise")
const USDT = artifacts.require("USDT");
const THMintFactory = artifacts.require("THMintFactory");
const THPeriodFactory = artifacts.require("THPeriodFactory");
const THTRDispatcherFactory = artifacts.require("THTRDispatcherFactory");
const THMint = artifacts.require("THMint");
const THPeriod = artifacts.require("THPeriod");
const THTRDispatcher = artifacts.require("THTRDispatcher");
const THTRDPeriodAmountFactory = artifacts.require("THTRDPeriodAmountFactory");
const THTRDPeriodAmount = artifacts.require("THTRDPeriodAmount");

contract("Test THTRDispatcher", (accounts)=>{
  context("init", ()=>{
    let price = {}
    let tr = {}
    let usdt = {}
    let i = 0
    let start_block = 0;
    let pool = {}
    it("init", async function(){
      sr = StepRecorder("ganache", "structure");

      start_block = await getBlockNumber();
      tr_factory = await THTokenRaiseFactory.deployed()
      usdt = await USDT.deployed()
      await usdt.issue(accounts[0], 100000000000000)
      await usdt.issue(accounts[1], 100000000000000)

      tx = await tr_factory.createTHTokenRaise(start_block, start_block + 10, 100000000, usdt.address, "20000000000000000000000", accounts[8])
      tr = await THTokenRaise.at(tx.logs[0].args.addr);

      await usdt.approve(tr.address, 1000000000000,{from:accounts[0]})
      await usdt.approve(tr.address, 1000000000000,{from:accounts[1]})

      await tr.raise(60000000, {from:accounts[0]})
      await tr.raise(15000000, {from:accounts[1]})

      pool = await TokenBankV2.deployed()
      pool_tl = await TrustList.at(await pool.trustlist())

      period_factory = await THPeriodFactory.deployed()
      tx = await period_factory.createTHPeriod(start_block + 11, 2, 1)
      th_period = await THPeriod.at(tx.logs[0].args.addr);

      tht_addr = sr.read("tht_token")
      tht = await ERC20Token.at(tht_addr);
      tlist = await TrustList.at(sr.read("tht_token_trustlist"))

      mint_factory = await THMintFactory.deployed()
      tx = await mint_factory.createTHMint(th_period.address, tht_addr, pool.address, "10000000000000000000000")
      th_mint = await THMint.at(tx.logs[0].args.addr);

      await tlist.add_trusted(th_mint.address)

      pa_factory = await THTRDPeriodAmountFactory.deployed()
      tx = await pa_factory.createTHTRDPeriodAmount(1, 2, th_mint.address, tr.address);
      pa = await THTRDPeriodAmount.at(tx.logs[0].args.addr)

      dis_factory = await THTRDispatcherFactory.deployed()
      tx = await dis_factory.createTHTRDispatcher(tht_addr, pool.address, th_period.address, tr.address, pa.address, 0, 10)
      th_dis = await THTRDispatcher.at(tx.logs[0].args.addr);

      await pool_tl.add_trusted(th_dis.address)

    })

    it('claim and mint', async function(){
      await expectRevert(tr.raise(45000000), "raise end");
      cb = await getBlockNumber();
      while (cb < await th_period.start_block()){
        await th_period.get_current_period();
      }
      await th_mint.mint();
      await th_dis.claim({from:accounts[0]});
      b0 = (await tht.balanceOf(accounts[0])).toString();
      expect(b0).to.equal("7200000000000000000000")

      await th_dis.claim({from:accounts[1]});
      b1 = (await tht.balanceOf(accounts[1])).toString();
      expect(b1).to.equal("1800000000000000000000")

      cb = await getBlockNumber();
      while (cb < start_block + 40){
        await usdt.transfer(accounts[0], 0, {from:accounts[0]});
        cb = await getBlockNumber();
      }
      await th_mint.mint()
      //block 30,------2,3,4,5,6,7,,,8,period 7
      await th_dis.claim({from:accounts[0]});
      b0 = (await tht.balanceOf(accounts[0])).toString();
      expect(b0).to.equal("16800000000000000000000")

      while (cb < start_block + 80){
        await usdt.transfer(accounts[0], 0, {from:accounts[0]});
        cb = await getBlockNumber();
      }
      //block 70, period 11,------2,3,4,5,6,7,8,9,10,11,,,12
      await expectRevert(th_dis.claim({from:accounts[1]}), "claim already end");

      bp = (await tht.balanceOf(pool.address)).toString();
      expect(bp).to.equal("51400000000000000000000")
    })
  })
})
