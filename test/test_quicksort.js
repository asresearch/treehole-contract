const {
  BN,
  constants,
  expectEvent,
  expectRevert,
} = require("@openzeppelin/test-helpers");
const { expect } = require("chai");
const QuickSort = artifacts.require("QuickSort");

contract("TestQuickSort", (accounts) => {
  let qsort;
  context("init", async () => {
    it("init", async () => {
      qsort = await QuickSort.deployed();
    });

    it("test for one", async () => {
      d1 = "0x3e0d3a43f4f45ba7a1234759c2ffa4028a44599d4ab29bec532bd2057c0f9141";
      ts = await qsort.sort([d1]);
    });

    it("test for two", async () => {
      d1 = "0x3e0d3a43f4f45ba7a1234759c2ffa4028a44599d4ab29bec532bd2057c0f9141";
      d2 = "0x362a609ab5a6eecafdb2289890bd7261871c04fb5d7323d4fc750f6444b067a1";
      ts = await qsort.sort([d1, d2]);
      expect(ts[0] > ts[1]).to.equal(true);
    });

    it("test for three", async () => {
      d1 = "0x3e0d3a43f4f45ba7a1234759c2ffa4028a44599d4ab29bec532bd2057c0f9141";
      d2 = "0x362a609ab5a6eecafdb2289890bd7261871c04fb5d7323d4fc750f6444b067a1";
      d3 = "0xa96efbe24c62572156caa514657d4a535101d2147337f41f51fcdfcf8f43a532";
      ts = await qsort.sort([d1, d2, d3]);
      expect(ts[0] > ts[1]).to.equal(true);
      expect(ts[1] > ts[2]).to.equal(true);
    });
  });
});
