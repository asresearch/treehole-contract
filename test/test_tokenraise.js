const { expect } = require('chai');

const { BN, constants, expectEvent, expectRevert  } = require('@openzeppelin/test-helpers');
const { keccak256, bufferToHex, toBuffer } = require('ethereumjs-util');


const getBlockNumber = require('./blockNumber')(web3)

const THTokenRaiseFactory = artifacts.require("THTokenRaiseFactory")
const THTokenRaise = artifacts.require("THTokenRaise")
const USDT = artifacts.require("USDT");

contract("Test Raise", (accounts)=>{
  context("init", ()=>{
    let price = {}
    let tr = {}
    let sr = {}
    let usdt = {}
    let i = 0
    let start_block = 0;
    it("init", async function(){
      start_block = await getBlockNumber();
      tr_factory = await THTokenRaiseFactory.deployed()
      usdt = await USDT.deployed()
      await usdt.issue(accounts[0], 100000000000)
      await usdt.issue(accounts[1], 100000000000)

      tx = await tr_factory.createTHTokenRaise(start_block, start_block + 20, 100000000, usdt.address, 20000, accounts[8])
      tr = await THTokenRaise.at(tx.logs[0].args.addr);

      await usdt.approve(tr.address, 100000000000,{from:accounts[0]})
      await usdt.approve(tr.address, 100000000000,{from:accounts[1]})
    })
    it('raise', async function(){
      await tr.raise(60000000, {from:accounts[0]})
      await tr.raise(15000000, {from:accounts[1]})
      b = await usdt.balanceOf(accounts[8])
      expect(b.toNumber()).to.equal(75000000);

      f  = await tr.user_proportion(accounts[0])
      f0 = f[0].toNumber()
      f1 = f[1].toNumber()
      expect(f0).to.equal(60000000)
      expect(f1).to.equal(75000000)

      f= await tr.get_share_fraction()
      f0 = f[0].toNumber()
      f1 = f[1].toNumber()
      expect(f0).to.equal(75000000)
      expect(f1).to.equal(125000000)

      s = (await tr.get_current_share()).toNumber()
      expect(s).to.equal(12000)

      p = (await tr.get_current_price()).toString()
      expect(p).to.equal('625000000000000000')
    })

    it('block', async function(){
      for(i = 0; i < 20; i++){
        await usdt.transfer(accounts[0], 0, {from:accounts[0]});
      }
      await expectRevert(tr.raise(45000000), "raise end");
      cb = await getBlockNumber();
      await tr.change_end_block(cb + 5);
      await tr.raise(45000000, {from:accounts[0]})

      b = await usdt.balanceOf(accounts[8])
      expect(b.toNumber()).to.equal(120000000);

      f = await tr.get_share_fraction()
      f0 = f[0].toNumber()
      f1 = f[1].toNumber()
      expect(f0).to.equal(1)
      expect(f1).to.equal(1)

      s = (await tr.get_current_share()).toNumber()
      expect(s).to.equal(20000)

      p = (await tr.get_current_price()).toString()
      expect(p).to.equal('600000000000000000')
    })
  })
})
