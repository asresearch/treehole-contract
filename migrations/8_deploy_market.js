const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const MerkleProof = artifacts.require("MerkleProof");
const SGXRequest = artifacts.require("SGXRequest");
const SGXOnChainResult = artifacts.require("SGXOnChainResult");
const SGXStaticData = artifacts.require("SGXStaticData");
const ECDSA = artifacts.require("ECDSA");
const SignatureVerifier = artifacts.require("SignatureVerifier");
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXProgramStoreFactory = artifacts.require("SGXProgramStoreFactory");
const SGXStaticDataMarketPlaceFactory = artifacts.require("SGXStaticDataMarketPlaceFactory");
const NaiveOwnerFactory = artifacts.require("NaiveOwnerFactory")

const {DHelper} = require("./util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  await dhelper.readOrCreateContract(SGXKeyVerifierFactory, [ECDSA]);
  await dhelper.readOrCreateContract(SGXProgramStoreFactory, []);
  await dhelper.readOrCreateContract(SGXStaticDataMarketPlaceFactory, [SafeMath, SafeERC20, ECDSA, SignatureVerifier, SGXStaticData])
  await dhelper.readOrCreateContract(NaiveOwnerFactory,[]);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};