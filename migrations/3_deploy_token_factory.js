const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const AddressArray = artifacts.require("AddressArray");
const { DHelper } = require("./util.js");
//const RDS = artifacts.require("RDS");

async function performMigration(deployer, network, accounts, dhelper) {
  console.log("dhelper", dhelper);
  await dhelper.readOrCreateContract(ERC20TokenFactory, [AddressArray]);
  //await dhelper.readOrCreateContract(RDS);
}

module.exports = function (deployer, network, accounts) {
  deployer
    .then(function () {
      console.log(DHelper);
      return performMigration(
        deployer,
        network,
        accounts,
        DHelper(deployer, network, accounts)
      );
    })
    .catch((error) => {
      console.log(error);
      process.exit(1);
    });
};
