const THPeriod_for_mine_test = artifacts.require("THPeriod_for_mine_test");
const Program_Proxy = artifacts.require("Program_Proxy");
const DataMarketPlace_for_mine_test = artifacts.require("DataMarketPlace_for_mine_test");
const THMiner = artifacts.require("THMiner");
const SafeMath = artifacts.require("SafeMath")
const Address = artifacts.require("Address")
const TokenBankV2 = artifacts.require("TokenBankV2");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20Token = artifacts.require("ERC20Token")
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const AddressArray = artifacts.require("AddressArray");



const {DHelper, StepRecorder} = require("./util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  proxy = await dhelper.readOrCreateContract(Program_Proxy, []);
  market = await dhelper.readOrCreateContract(DataMarketPlace_for_mine_test, [],proxy.address);
  market2 =await dhelper.readOrCreateContract(DataMarketPlace_for_mine_test, [],proxy.address);
  sr =  new StepRecorder(network, "market")
  // sr2 =  new StepRecorder(network, "market2")

  period = await dhelper.readOrCreateContract(THPeriod_for_mine_test, []);
  tlist_factory = await TrustListFactory.deployed()
  tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
  pool_tlist = await TrustList.at(tx.logs[0].args.addr);

  pool = await dhelper.readOrCreateContract(TokenBankV2, [SafeMath], "tht pool" )
  await pool.changeTrustList(pool_tlist.address)
  usdc = await ERC20Token.at(sr.read("token"))
  token_factory = await dhelper.readOrCreateContract(ERC20TokenFactory, [AddressArray]);
  tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000',
      0, "USDC", 8, "USDC", true,)
  usdc2 = await ERC20Token.at(tx.logs[0].args._cloneToken);
  miner = await dhelper.readOrCreateContract(THMiner, [SafeMath, Address], market.address, pool.address, period.address, usdc.address);
  await pool_tlist.add_trusted(miner.address);
  await pool_tlist.add_trusted(accounts[0]);
  await miner.changeTrustList(pool_tlist.address)
  sr.write('market2',market2.address)
  sr.write('token2',usdc2.address)

}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
