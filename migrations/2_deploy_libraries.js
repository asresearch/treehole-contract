const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const Miscellaneous = artifacts.require("Miscellaneous");
const MerkleProof = artifacts.require("MerkleProof");
const ECDSA = artifacts.require("ECDSA");
const {DHelper} = require("./util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  console.log("dhelper", dhelper)
  await dhelper.readOrCreateContract(SafeMath);
  await dhelper.readOrCreateContract(Address)
  await dhelper.readOrCreateContract(AddressArray)
  await dhelper.readOrCreateContract(SafeERC20);
  await dhelper.readOrCreateContract(Miscellaneous, [SafeMath]);
  await dhelper.readOrCreateContract(MerkleProof);
  await dhelper.readOrCreateContract(ECDSA);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
