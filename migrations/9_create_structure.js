const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const Miscellaneous = artifacts.require("Miscellaneous");
const MerkleProof = artifacts.require("MerkleProof");
const SGXRequest = artifacts.require("SGXRequest");
const SGXOnChainResult = artifacts.require("SGXOnChainResult");
const SGXStaticData = artifacts.require("SGXStaticData");
const ECDSA = artifacts.require("ECDSA");
const SignatureVerifier = artifacts.require("SignatureVerifier");
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXProgramStoreFactory = artifacts.require("SGXProgramStoreFactory");
const SGXStaticDataMarketPlaceFactory = artifacts.require(
  "SGXStaticDataMarketPlaceFactory"
);
const SGXKeyVerifier = artifacts.require("SGXKeyVerifier");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const SGXProgramStore = artifacts.require("SGXProgramStore");
const SGXStaticDataMarketPlace = artifacts.require("SGXStaticDataMarketPlace");
const SGXOnChainResultMarket = artifacts.require("SGXOnChainResultMarket");
const SGXOnChainResultMarketImplV1 = artifacts.require(
  "SGXOnChainResultMarketImplV1"
);
const SGXDataMarketCommon = artifacts.require("SGXDataMarketCommon");
const SGXDataMarketCommonImplV1 = artifacts.require(
  "SGXDataMarketCommonImplV1"
);
const USDT = artifacts.require("USDT");
const NaiveOwnerFactory = artifacts.require("NaiveOwnerFactory");
const NaiveOwner = artifacts.require("NaiveOwner");

// multi-onchain related
const SGXMultiOnChainResultMarket = artifacts.require(
  "SGXMultiOnChainResultMarket"
);
const SGXVirtualDataImplV1 = artifacts.require("SGXVirtualDataImplV1");
const QuickSort = artifacts.require("QuickSort");

// offchain related
const SGXOffChainResultMarket = artifacts.require("SGXOffChainResultMarket");
const SGXOffChainResultMarketImplV1 = artifacts.require(
  "SGXOffChainDataMarketImplV1"
);
const SGXOffChainResult = artifacts.require("SGXOffChainResult");

// multi-offchain related
const SGXMultiOffChainResultMarket = artifacts.require(
  "SGXMultiOffChainResultMarket"
);

const { constants } = require("@openzeppelin/test-helpers");
// utils
const { DHelper, StepRecorder } = require("./util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  vf = await dhelper.readOrCreateContract(SGXKeyVerifierFactory, [ECDSA]);
  sf = await dhelper.readOrCreateContract(SGXProgramStoreFactory, []);
  mf = await dhelper.readOrCreateContract(SGXStaticDataMarketPlaceFactory, [
    SafeMath,
    SafeERC20,
    ECDSA,
    SignatureVerifier,
    SGXStaticData,
  ]);
  nf = await dhelper.readOrCreateContract(NaiveOwnerFactory, []);

  tx = await nf.createNaiveOwner();
  owner_proxy = await NaiveOwner.at(tx.logs[0].args.addr);

  sr = new StepRecorder(network, "market");
  tx = await vf.createSGXKeyVerifier();
  verifier = await SGXKeyVerifier.at(tx.logs[0].args.addr);
  tx = await sf.createSGXProgramStore(owner_proxy.address);
  pstore = await SGXProgramStore.at(tx.logs[0].args.addr);

  usdt = await dhelper.readOrCreateContract(USDT);
  token_factory = await dhelper.readOrCreateContract(ERC20TokenFactory, [
    AddressArray,
  ]);
  tlist_factory = await dhelper.readOrCreateContract(TrustListFactory, [
    AddressArray,
  ]);
  tx = await tlist_factory.createTrustList([
    "0x0000000000000000000000000000000000000000",
  ]);
  tlist = await TrustList.at(tx.logs[0].args.addr);
  await tlist.add_trusted(accounts[0]);

  tx = await token_factory.createCloneToken(
    "0x0000000000000000000000000000000000000000",
    0,
    "USDC",
    8,
    "USDC",
    true
  );
  usdc = await ERC20Token.at(tx.logs[0].args._cloneToken);
  await usdc.changeTrustList(tlist.address);
  await usdc.generateTokens(accounts[0], "10000000000000000");

  tx = await mf.createSGXStaticDataMarketPlace(
    pstore.address,
    owner_proxy.address,
    usdc.address
  );
  market = await SGXStaticDataMarketPlace.at(tx.logs[0].args.addr);
  await market.changeTrustList(tlist.address);

  tx = await mf.createSGXStaticDataMarketPlace(
    pstore.address,
    owner_proxy.address,
    constants.ZERO_ADDRESS
  );
  market_no_payment = await SGXStaticDataMarketPlace.at(tx.logs[0].args.addr);
  await market_no_payment.changeTrustList(tlist.address);

  onchain_impl = await dhelper.readOrCreateContract(
    SGXOnChainResultMarketImplV1,
    [SafeMath, ECDSA, SignatureVerifier, SGXStaticData]
  );
  onchain_market = await dhelper.readOrCreateContract(SGXOnChainResultMarket, [
    SafeERC20,
  ]);

  await onchain_market.changeDataLib(onchain_impl.address);
  await onchain_market.changeMarket(market.address);
  await tlist.add_trusted(onchain_market.address);

  common_impl = await dhelper.readOrCreateContract(SGXDataMarketCommonImplV1, [
    ECDSA,
    SignatureVerifier,
    SGXStaticData,
  ]);
  common_market = await dhelper.readOrCreateContract(SGXDataMarketCommon, []);
  await common_market.changeDataLib(common_impl.address);
  await common_market.changeMarket(market.address);
  await tlist.add_trusted(common_market.address);

  // deploy multi-onchain related
  virtual_data_impl = await dhelper.readOrCreateContract(SGXVirtualDataImplV1, [
    SafeMath,
    QuickSort,
    SGXStaticData,
  ]);
  multi_onchain_market = await dhelper.readOrCreateContract(
    SGXMultiOnChainResultMarket,
    [SafeERC20]
  );
  await multi_onchain_market.changeDataLib(onchain_impl.address);
  await multi_onchain_market.changeVirtualDataLib(virtual_data_impl.address);
  await multi_onchain_market.changeMarket(market.address);
  await tlist.add_trusted(multi_onchain_market.address);

  // deploy offchain related
  offchain_impl = await dhelper.readOrCreateContract(
    SGXOffChainResultMarketImplV1,
    [SafeMath, ECDSA, SignatureVerifier, SGXStaticData, SGXOffChainResult]
  );
  offchain_result_market = await dhelper.readOrCreateContract(
    SGXOffChainResultMarket,
    [SafeERC20, Miscellaneous]
  );

  await offchain_result_market.changeDataLib(offchain_impl.address);
  await offchain_result_market.changeMarket(market.address);
  await tlist.add_trusted(offchain_result_market.address);

  // deploy multi-offchain related
  virtual_data_impl = await dhelper.readOrCreateContract(SGXVirtualDataImplV1, [
    SafeMath,
    QuickSort,
    SGXStaticData,
  ]);
  multi_offchain_market = await dhelper.readOrCreateContract(
    SGXMultiOffChainResultMarket,
    [SafeERC20, Miscellaneous]
  );
  await multi_offchain_market.changeDataLib(offchain_impl.address);
  await multi_offchain_market.changeVirtualDataLib(virtual_data_impl.address);
  await multi_offchain_market.changeMarket(market.address);
  await tlist.add_trusted(multi_offchain_market.address);

  // step recorder
  sr.write("token", usdc.address);
  sr.write("verifier", verifier.address);
  sr.write("program-store", pstore.address);
  sr.write("market", market.address);
  sr.write("market-no-payment", market_no_payment.address);
  sr.write("onchain-market", onchain_market.address);
  sr.write("onchain-market-impl", onchain_impl.address);
  sr.write("common-market", common_market.address);
  sr.write("common-market-impl", common_impl.address);
  sr.write("owner-proxy", owner_proxy.address);
  // step record multi-onchain related
  sr.write("multi-onchain-market", multi_onchain_market.address);
  sr.write("virtual-data-impl", virtual_data_impl.address);
  // step record offchain related
  sr.write("offchain-result-market", offchain_result_market.address);
  sr.write("offchain-market-impl", offchain_impl.address);
  // step record multi-offchain related
  sr.write("multi-offchain-market", multi_offchain_market.address);
}

module.exports = function (deployer, network, accounts) {
  deployer
    .then(function () {
      console.log(DHelper);
      return performMigration(
        deployer,
        network,
        accounts,
        DHelper(deployer, network, accounts)
      );
    })
    .catch((error) => {
      console.log(error);
      process.exit(1);
    });
};
