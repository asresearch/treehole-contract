const { DHelper } = require("./util.js");
const QuickSort = artifacts.require("QuickSort");
const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const MerkleProof = artifacts.require("MerkleProof");
const ECDSA = artifacts.require("ECDSA");
const SGXRequest = artifacts.require("SGXRequest");
const SGXOnChainResult = artifacts.require("SGXOnChainResult");
const SGXOffChainResult = artifacts.require("SGXOffChainResult");
const SGXStaticData = artifacts.require("SGXStaticData");
const SignatureVerifier = artifacts.require("SignatureVerifier");

async function performMigration(deployer, network, accounts, dhelper) {
  console.log("dhelper", dhelper);
  qs = await dhelper.readOrCreateContract(QuickSort);
  await dhelper.readOrCreateContract(SafeMath);
  await dhelper.readOrCreateContract(Address);
  await dhelper.readOrCreateContract(AddressArray);
  await dhelper.readOrCreateContract(SafeERC20);
  await dhelper.readOrCreateContract(MerkleProof);
  await dhelper.readOrCreateContract(ECDSA);
  await dhelper.readOrCreateContract(SignatureVerifier, [ECDSA]);
  await dhelper.readOrCreateContract(SGXRequest, [
    SafeMath,
    SafeERC20,
    ECDSA,
    SignatureVerifier,
  ]);
  await dhelper.readOrCreateContract(SGXOnChainResult, [
    SafeMath,
    SafeERC20,
    ECDSA,
    SignatureVerifier,
  ]);
  await dhelper.readOrCreateContract(SGXOffChainResult, [
    SafeMath,
    SafeERC20,
    ECDSA,
    SignatureVerifier,
  ]);
  await dhelper.readOrCreateContract(SGXStaticData, [
    SGXRequest,
    SGXOnChainResult,
    SGXOffChainResult,
    SafeMath,
    ECDSA,
  ]);
}

module.exports = function (deployer, network, accounts) {
  deployer
    .then(function () {
      console.log(DHelper);
      return deployer, network, accounts, DHelper(deployer, network, accounts);
    })
    .catch((error) => {
      console.log(error);
      process.exit(1);
    });
};
