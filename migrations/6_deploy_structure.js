const { DHelper, StepRecorder } = require("./util.js");
const TokenBankV2 = artifacts.require("TokenBankV2");
const TokenBankV2Factory = artifacts.require("TokenBankV2Factory");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const AddressArray = artifacts.require("AddressArray");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const SafeMath = artifacts.require("SafeMath");
const SafeERC20 = artifacts.require("SafeERC20");
const THMintFactory = artifacts.require("THMintFactory");
const THPeriodFactory = artifacts.require("THPeriodFactory");
const THTRDispatcherFactory = artifacts.require("THTRDispatcherFactory");
const THTokenRaiseFactory = artifacts.require("THTokenRaiseFactory");
const THTRDPeriodAmountFactory = artifacts.require("THTRDPeriodAmountFactory");

async function performMigration(deployer, network, accounts, dhelper) {
  token_factory = await dhelper.readOrCreateContract(ERC20TokenFactory, [
    AddressArray,
  ]);
  tlist_factory = await dhelper.readOrCreateContract(TrustListFactory, [
    AddressArray,
  ]);

  mint_factory = await dhelper.readOrCreateContract(THMintFactory, [SafeMath]);
  period_factory = await dhelper.readOrCreateContract(THPeriodFactory);
  dispatcher_factory = await dhelper.readOrCreateContract(
    THTRDispatcherFactory,
    [SafeMath]
  );
  tokenraise_factory = await dhelper.readOrCreateContract(THTokenRaiseFactory, [
    SafeERC20,
    SafeMath,
  ]);

  pa_factory = await dhelper.readOrCreateContract(THTRDPeriodAmountFactory, [
    SafeMath,
  ]);

  sr = new StepRecorder(network, "structure");
  tx = await tlist_factory.createTrustList([
    "0x0000000000000000000000000000000000000000",
  ]);
  tlist = await TrustList.at(tx.logs[0].args.addr);
  await tlist.add_trusted(accounts[0]);

  tx = await token_factory.createCloneToken(
    "0x0000000000000000000000000000000000000000",
    0,
    "Treehole Token",
    18,
    "THT",
    true
  );
  tht_token = await ERC20Token.at(tx.logs[0].args._cloneToken);
  await tht_token.changeTrustList(tlist.address);

  sr.write("tht_token_trustlist", tlist.address);
  sr.write("tht_token", tht_token.address);

  tx = await tlist_factory.createTrustList([
    "0x0000000000000000000000000000000000000000",
  ]);
  pool_tlist = await TrustList.at(tx.logs[0].args.addr);

  pool = await dhelper.readOrCreateContract(
    TokenBankV2,
    [SafeMath],
    "tht pool"
  );
  await pool.changeTrustList(pool_tlist.address);

  sr.write("pool", pool.address);
}

module.exports = function (deployer, network, accounts) {
  deployer
    .then(function () {
      console.log(DHelper);
      return performMigration(
        deployer,
        network,
        accounts,
        DHelper(deployer, network, accounts)
      );
    })
    .catch((error) => {
      console.log(error);
      process.exit(1);
    });
};
