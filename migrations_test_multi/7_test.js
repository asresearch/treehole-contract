const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const MerkleProof = artifacts.require("MerkleProof");
const SGXRequest = artifacts.require("SGXRequest");
const SGXOnChainResult = artifacts.require("SGXOnChainResult");
const SGXStaticData = artifacts.require("SGXStaticData");
const ECDSA = artifacts.require("ECDSA");
const SignatureVerifier = artifacts.require("SignatureVerifier");
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXProgramStoreFactory = artifacts.require("SGXProgramStoreFactory");
const SGXStaticDataMarketPlaceFactory = artifacts.require("SGXStaticDataMarketPlaceFactory");
const SGXKeyVerifier = artifacts.require("SGXKeyVerifier");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const SGXProgramStore = artifacts.require("SGXProgramStore");
const SGXStaticDataMarketPlace = artifacts.require("SGXStaticDataMarketPlace");
const SGXOnChainResultMarket = artifacts.require("SGXOnChainResultMarket")
const SGXOnChainResultMarketImplV1 = artifacts.require("SGXOnChainResultMarketImplV1")
const SGXMultiOnChainResultMarket = artifacts.require("SGXMultiOnChainResultMarket")
const SGXDataMarketCommon = artifacts.require("SGXDataMarketCommon");
const SGXDataMarketCommonImplV1 = artifacts.require("SGXDataMarketCommonImplV1")
const PaymentPool = artifacts.require("PaymentPool")
const TestQuickSort = artifacts.require("TestQuickSort")
const fs = require('fs')

const {DHelper, StepRecorder, SoEVerifier} = require("../util.js");

async function test_submit_result(deployer, network, accounts, dhelper){
  sr =  StepRecorder(network, "market")
  paysys = StepRecorder(network, "payment-system")
  cny = await ERC20Token.at(paysys.read('CNY'))

  ppool_tlist = await TrustList.at(paysys.read("pool_tl"))
  market = await SGXStaticDataMarketPlace.at(sr.read("market"))
  multi_onchain_market = await SGXMultiOnChainResultMarket.at(sr.read('multi-onchain-market'))
  pstore = await SGXProgramStore.at(sr.read("program-store"))
  common_impl = await SGXDataMarketCommonImplV1.at(sr.read("common-market-impl"))
  common_market = await SGXDataMarketCommon.at(sr.read("common-market"))

  input_json = JSON.parse(fs.readFileSync('/root/findpersonparser_input.json'))
  output_json = JSON.parse(fs.readFileSync('/root/findpersonparser_output.json'))

  // upload_program
  tx = await pstore.upload_program("test_url", 0, '0x' + input_json.parser_enclave_hash)
  program_hash = tx.logs[0].args.hash
  //console.log("upload program, program hash:", program_hash)


  // createStaticData
  dhash1 = '0x' + input_json.input_data[0].input_data_hash
  pkey1 = '0x' + input_json.input_data[0]['public-key']
  let hash_sig1 = '0xd90d6a3685a78bcf71cd8d3ea785a7458ba53a762989b97a92b0d0ea105ed99e15b93f998d4eed249b23e842bcddcd0c59d0d4b8a41c34229ebcef94ae2e664a1b';
  //we use program_hash as format_lib_hash, it's a mock
  tx = await common_market.createStaticData(dhash1, "test env", 0, pkey1, hash_sig1);
  data_vhash1 = tx.logs[0].args.vhash
  //console.log("create static data, data onchain hash1:", data_vhash1)


  // createStaticData
  dhash2 = '0x' + input_json.input_data[1].input_data_hash
  pkey2 = '0x' + input_json.input_data[1]['public-key']
  let hash_sig2 = '0x0a9580c0f1c7f0a5de6e314151437498dbdc171fc46dd53950bcc037ca1284330db91c323beacc431af4332310806213872b95334277471da5de10bd809e28d31c';
  //we use program_hash as format_lib_hash, it's a mock
  tx = await common_market.createStaticData(dhash2, "test env", 0, pkey2, hash_sig2);
  data_vhash2 = tx.logs[0].args.vhash
  //console.log("create static data, data onchain hash2:", data_vhash2)


  // requestOnChain
  let secret = '0x' + input_json.shu_info.encrypted_shu_skey;
  let input = '0x' + input_json.param.param_data;
  let forward_sig = '0x' + input_json.shu_info.shu_forward_signature;
  let gas_price = 1;
  let pkey = '0x' + input_json.shu_info.shu_pkey

  await cny.approve(multi_onchain_market.address, 0);
  await cny.approve(multi_onchain_market.address, 1000000000000000)
  //cny_balance = await cny.balanceOf(accounts[0])
  //cny_allowance = await cny.allowance(accounts[0], multi_onchain_market.address)
  //console.log('balance:', cny_balance.toString())
  //console.log('allowance:', cny_allowance.toString())
  tx = await multi_onchain_market.requestOnChain([data_vhash1, data_vhash2], {secret:secret,input:input,forward_sig:forward_sig,program_hash:program_hash,pkey:pkey}, gas_price, 200000);
  //console.log('tx logs length: ', tx.logs.length)
  //console.log('tx logs: ', tx.logs)
  request_hash = tx.logs[1].args.request_hash
  //console.log("request onchain, request hash:", request_hash)
  confirm_hash = tx.logs[2].args.hash

  let log0 = tx.logs[0]
  //console.log('vhash: ', log0.args.vhash)
  //console.log('vhashes: ', log0.args.vhashes)
  let data_vhash = tx.logs[0].args.vhash

  tx = await multi_onchain_market.transferCommit(confirm_hash, true)
  //console.log("transfer commit, confirm hash:", confirm_hash)

  let result = '0x' + output_json.encrypted_result;
  let result_signature = '0x' + output_json.result_signature;

  console.log('input:', input)
  tx = await market.getDataInfo(data_vhash)
  console.log('data info:', tx)
  console.log('data hash:', tx.data_hash)
  tx = await pstore.get_program_info(program_hash)
  console.log('enclave hash:', tx.enclave_hash)
  console.log('result:', result)

  console.log('result signature:', result_signature)
  info = await market.getRequestInfo1(data_vhash, request_hash);
  console.log('pkey4v:', info.pkey4v);

  tx = await multi_onchain_market.submitOnChainResult(data_vhash, request_hash, 0, result, result_signature);
  console.log("submit result done!")
  confirm_hash = tx.logs[1].args.hash
  await multi_onchain_market.transferCommit(confirm_hash, true)
}

async function test_qsort(deployer, network, accounts, dhelper){
  sr =  StepRecorder(network, "market")
  qs = await TestQuickSort.at(sr.read("qsort"))
  data_json = JSON.parse(fs.readFileSync('/root/data.json'))
  //console.log(data_json)
  for (i = 0; i < data_json.data.length; i++) {
    datahash1 = data_json.data[i].datahash1
    datahash2 = data_json.data[i].datahash2
    sorted_data = await qs.sort(['0x'+datahash1, '0x'+datahash2])
    console.log(sorted_data[0]+','+sorted_data[1])
  }
  console.log(tx)
}

async function performMigration(deployer, network, accounts, dhelper) {
  await test_submit_result(deployer, network, accounts, dhelper);
  //await test_qsort(deployer, network, accounts, dhelper);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
