const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const MerkleProof = artifacts.require("MerkleProof");
const SGXRequest = artifacts.require("SGXRequest");
const SGXOnChainResult = artifacts.require("SGXOnChainResult");
const SGXStaticData = artifacts.require("SGXStaticData");
const ECDSA = artifacts.require("ECDSA");
const SignatureVerifier = artifacts.require("SignatureVerifier");
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXProgramStoreFactory = artifacts.require("SGXProgramStoreFactory");
const SGXStaticDataMarketPlaceFactory = artifacts.require("SGXStaticDataMarketPlaceFactory");
const SGXKeyVerifier = artifacts.require("SGXKeyVerifier");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const SGXProgramStore = artifacts.require("SGXProgramStore");
const SGXStaticDataMarketPlace = artifacts.require("SGXStaticDataMarketPlace");
const SGXOnChainResultMarket = artifacts.require("SGXOnChainResultMarket")
const SGXOnChainResultMarketImplV1 = artifacts.require("SGXOnChainResultMarketImplV1")
const SGXDataMarketCommon = artifacts.require("SGXDataMarketCommon");
const SGXDataMarketCommonImplV1 = artifacts.require("SGXDataMarketCommonImplV1")
const PaymentPool = artifacts.require("PaymentPool")

const {DHelper, StepRecorder, SoEVerifier} = require("../util.js");

async function test_submit_result(deployer, network, accounts, dhelper){
  if(!network.includes('ganache')){
    return;
  }

  sr =  StepRecorder(network, "market")
  paysys = StepRecorder(network, "payment-system")
  cny = await ERC20Token.at(paysys.read('CNY'))
  //cny = await ERC20Token.at(sr.read('token'))

  //verifier = await SGXKeyVerifier.at(sr.read("verifier"))
  ppool_tlist = await TrustList.at(paysys.read("pool_tl"))
  market = await SGXStaticDataMarketPlace.at(sr.read("market"))
  onchain_impl = await SGXOnChainResultMarketImplV1.at(sr.read("onchain-market-impl"))
  onchain_market = await SGXOnChainResultMarket.at(sr.read('onchain-market'))
  pstore = await SGXProgramStore.at(sr.read("program-store"))
  common_impl = await SGXDataMarketCommonImplV1.at(sr.read("common-market-impl"))
  common_market = await SGXDataMarketCommon.at(sr.read("common-market"))

  //await verifier.set_verifier_addr("0xf4267391072B27D76Ed8f2A9655BCf5246013F2d", true)
  tx = await pstore.upload_program("test_url", 0, "0x7b0ea722d13a3965a5792d1e7efdf2c9228a578aa935c901b8b9e704c676ccfd")
  program_hash = tx.logs[0].args.hash
  console.log("upload program, program hash:", program_hash)



  dhash = "0xa6f468b0f1c830a7e26ccecb2d5990ad3c27004bf6fc05ea53eda73c83f4cdc2"
  pkey = '0x9fa8c59686a906ee9afa0fe1a052786c91840e3233d86e6594386b1c6d2ba16e3277087a617a380526abe2bbcfdd22ad7496aeb46b6a665c050b71e5abbf34dd'
  let hash_sig = '0xf69771a2f3a514653c312834d46344bcf139ecfef0b5ca5f5f58e81bb22f184c0786a3fa23533c0ae215e40f7abaefa27a9162d42cb0ccc233ff1495a0ce61371b';
      //we use program_hash as format_lib_hash, it's a mock
  tx = await common_market.createStaticData(dhash, "test env", 0, pkey, hash_sig);
      //console.log("tx log: ", tx)
  data_vhash = tx.logs[0].args.vhash
  console.log("create static data, data onchain hash:", data_vhash)


  let secret = '0xc08577a2ac1969778c3ec9953362dc5aa8e8feb318f48e59c17757748f31f877096f380d07410c1cdc55d29285eed7802b99269eeaebbf995d9204bb5267ea5d0824a8264ebca91005bfc4bf5797383ab46571a27052d2bdef88e8392b491cb95dd35690c3e1fab8df22bdac160d73dda5da22a26f153475468d243e';
  let input = "0xc17fdbeac43335f37cd390ae1262dc228361a5cd8512274776ed6b8e2a0b157d083c59c7d402cb67f39ec682681fb28bfa6c2133f9febe979a6b073f7962de40398cfca243e58543a063a8bef1aba689e35d854213ba78b322a8532d2f706510d30e6063e88d354423ea300eefdcf3be806f9d804055bc93969a";
  let forward_sig = '0x90432ef3bca5676592e767fe15da64d83e55ff6657b98a030515903bb96495cf4e49dbec678f2c17810b15df66f35609a401150362ec23cdcb0b04013ac3cde91b';
  let gas_price = 1;
  pkey = "0x539910104a790e611c9b3aed8af4b5035a43b939d1af896c727da59f05666cb136c0ae158e9c2470ce498cc90d2596b784275f1f761354241746fdc7f8159d9c"

  await cny.approve(onchain_market.address, 0);
  await cny.approve(onchain_market.address, 1000000000000000)
  tx = await onchain_market.requestOnChain(data_vhash, secret, input, forward_sig, program_hash, gas_price, pkey, 200000);
  request_hash = tx.logs[0].args.request_hash
  console.log("request onchain, request hash:", request_hash)
  confirm_hash = tx.logs[1].args.hash

  paypool = await PaymentPool.at(paysys.read('paypool'))
  await paypool.transferCommit(confirm_hash, true)
  console.log("transfer commit, confirm hash:", confirm_hash)


  let result = '0x9fed81a044027bb2cedf358128c6949073faa8fa42ec10363fa0f097bf4c8062cc4cca63760cc4fbfa8201d23eb81cd3cf13fd9c05293ad51f12a0aef554d2beb96819e242e68546c891c6ea9f656c3b2845fb5ef13245d4f2cd4bfdad0b661bde75e94032c9fed0f233bb7505855e31359ebae7a24ef8b1f9';
  let result_signature = '0xe6802a2cf3a47e8a81463d52149748fe532f59b2d9c13195950e574b4b1d93607dc4318e99341d0edec3e13b5629a16ca559829f9351b14b90743e483aa194801b';
  //tx = await onchain_market.submitOnChainResult(data_vhash, request_hash, 0, result, result_signature);
  //console.log("submit result done!")
  //confirm_hash = tx.logs[1].args.hash
  //await paypool.transferCommit(confirm_hash, true)
}

async function test_submit_reject(deployer, network, accounts, dhelper){
  if(!network.includes('ganache')){
    return;
  }

  sr =  StepRecorder(network, "market")
  paysys = StepRecorder(network, "payment-system")
  cny = await ERC20Token.at(paysys.read('CNY'))

  //verifier = await SGXKeyVerifier.at(sr.read("verifier"))
  ppool_tlist = await TrustList.at(paysys.read("pool_tl"))
  market = await SGXStaticDataMarketPlace.at(sr.read("market"))
  onchain_impl = await SGXOnChainResultMarketImplV1.at(sr.read("onchain-market-impl"))
  onchain_market = await SGXOnChainResultMarket.at(sr.read('onchain-market'))
  pstore = await SGXProgramStore.at(sr.read("program-store"))
  common_impl = await SGXDataMarketCommonImplV1.at(sr.read("common-market-impl"))
  common_market = await SGXDataMarketCommon.at(sr.read("common-market"))

  //await verifier.set_verifier_addr("0xf4267391072B27D76Ed8f2A9655BCf5246013F2d", true)
  tx = await pstore.upload_program("test_url", 0, "0x3e0d3a43f4f45ba7a1234759c2ffa4028a44599d4ab29bec532bd2057c0f9141")
  program_hash = tx.logs[0].args.hash



  dhash = "0x3e0d3a43f4f45ba7a1234759c2ffa4028a44599d4ab29bec532bd2057c0f9141"
  pkey = '0x362a609ab5a6eecafdb2289890bd7261871c04fb5d7323d4fc750f6444b067a12a96efbe24c62572156caa514657d4a535101d2147337f41f51fcdfcf8f43a53'
  //let pkey_sig = '0xd9b0a2d2a1c669c7cfd40e1bb71041597140cfff38ec36ff4027405bc18e0b0f2109b354641feda4c4c38bc17836e8b1d15b2054b0c359347595783f9d0664021b'
  let hash_sig = '0xe014387b04dddc1a2ec75b42f2f8313395e1c391a8fb3bd1544d82071e1c1ae665ce132eac6f6f57f900c1d781714bddd8297b770d11714aa6e5ad4bdd9516eb1b';
  //we use program_hash as format_lib_hash, it's a mock
  tx = await common_market.createStaticData(dhash, "test env", 0, pkey, hash_sig);
  //console.log("tx log: ", tx)
  data_vhash = tx.logs[0].args.vhash


  let secret = '0x9fbe7febcd5c9bd7e50a51ca03652271fd0455a800bf331e02a85e6223e8493905e8282f68e70b56eef6e51a3c69f453d8c5dcdb3ac36144e5e810a41202a478d4a6b4d57db8bb6ea0931c5907037eaac6ed9980426479ec401157bd649bc33e3eefbec1354fb8aa4feca5f1681aeed1581c201ab7f73c636051f63f4221183f51f2c02591a47322cf57055e18e63aa246f5c6d9ab2c28b233b8d807b843e9111cc110dfdc8ffcf7ad8afffd4848832a84';
  let input = "0xaf4145ab19e5a354c2118032d4a6ca81ac4ddf1ffcd0e227cd648fb1701d95f917e31481e38d832d38f0ffbbba80228ab1ee9c05e88f997cae4354677f4fe0b9dce23ca07309cddc32dd0997517aec00687314";
  let forward_sig = '0x0ce588a6d240a4c6da1b9c887c32576fd4eb43b170d42d4fea01e8cdfc50be634fdce867af14b76f8fbade6ca03a42b74ea855a1f12e80ff71e2dd5f79879f6d1c';
  let gas_price = 1;
  pkey = "0x5d7ee992f48ffcdb077c2cb57605b602bd4029faed3e91189c7fb9fccc72771e45b7aa166766e2ad032d0a195372f5e2d20db792901d559ab0d2bfae10ecea97"

  await cny.approve(onchain_market.address, 0);
  await cny.approve(onchain_market.address, 1000000000000000)
  tx = await onchain_market.requestOnChain(data_vhash, secret, input, forward_sig, program_hash, gas_price, pkey, 200000);
  request_hash = tx.logs[0].args.request_hash
  confirm_hash = tx.logs[1].args.hash

  paypool = await PaymentPool.at(paysys.read('paypool'))
  await paypool.transferCommit(confirm_hash, true)


  let result = '0x566df6fc662a2cd177ee05de6dde43e0b1aa2798866b1e1656ff5d2c38171a1e76de34448e9f527a04754942290a87e3c50de136d22c8b196e7db089f6edd8b2643830b1e8b8051ec75e55d4d8eabcce679b18c41aec0dad05df7e8c0e'
  let result_signature = '0xd83b1a6cddc99d5564c0a0dd38d66323055e86f6b226d5c588ac8d1fb09e5d056d41972873c41e286df1246cc667d6cf17a1699ec854921005304d6f0a02fc0f1b';
  //tx = await common_market.rejectRequest(data_vhash, request_hash);
  //confirm_hash = tx.logs[1].args.hash
  //await paypool.transferCommit(confirm_hash, true)
}

async function performMigration(deployer, network, accounts, dhelper) {
  await test_submit_result(deployer, network, accounts, dhelper);
  await test_submit_reject(deployer, network, accounts, dhelper);
}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
