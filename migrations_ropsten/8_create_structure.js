const SafeMath = artifacts.require("SafeMath");
const AddressArray = artifacts.require("AddressArray");
const Address = artifacts.require("Address");
const SafeERC20 = artifacts.require("SafeERC20");
const MerkleProof = artifacts.require("MerkleProof");
const SGXRequest = artifacts.require("SGXRequest");
const SGXOnChainResult = artifacts.require("SGXOnChainResult");
const SGXStaticData = artifacts.require("SGXStaticData");
const ECDSA = artifacts.require("ECDSA");
const SignatureVerifier = artifacts.require("SignatureVerifier");
const SGXKeyVerifierFactory = artifacts.require("SGXKeyVerifierFactory");
const SGXProgramStoreFactory = artifacts.require("SGXProgramStoreFactory");
const SGXStaticDataMarketPlaceFactory = artifacts.require("SGXStaticDataMarketPlaceFactory");
const SGXKeyVerifier = artifacts.require("SGXKeyVerifier");
const TrustListFactory = artifacts.require("TrustListFactory");
const TrustList = artifacts.require("TrustList");
const ERC20TokenFactory = artifacts.require("ERC20TokenFactory");
const ERC20Token = artifacts.require("ERC20Token");
const SGXProgramStore = artifacts.require("SGXProgramStore");
const SGXStaticDataMarketPlace = artifacts.require("SGXStaticDataMarketPlace");
const SGXOnChainResultMarket = artifacts.require("SGXOnChainResultMarket")
const SGXOnChainResultMarketImplV1 = artifacts.require("SGXOnChainResultMarketImplV1")
const SGXDataMarketCommon = artifacts.require("SGXDataMarketCommon");
const SGXDataMarketCommonImplV1 = artifacts.require("SGXDataMarketCommonImplV1")
const NaiveOwnerFactory = artifacts.require("NaiveOwnerFactory")
const NaiveOwner = artifacts.require("NaiveOwner")

const {DHelper, StepRecorder, SoEVerifier} = require("../util.js");

async function performMigration(deployer, network, accounts, dhelper) {
  await dhelper.readOrCreateContract(NaiveOwnerFactory,[]);
  nf = await dhelper.readOrCreateContract(NaiveOwnerFactory, []);
  tx = await nf.createNaiveOwner();
  owner_proxy = await NaiveOwner.at(tx.logs[0].args.addr)

  sr =  StepRecorder(network, "market")
  paysys = StepRecorder(network, "payment-system")
  //verifier = await dhelper.readOrCreateContract(SGXKeyVerifier, [ECDSA])
  //sr.write("verifier", verifier.address)

  await dhelper.readOrCreateContract(SGXProgramStoreFactory, []);
  sf = await dhelper.readOrCreateContract(SGXProgramStoreFactory, []);
  tx = await sf.createSGXProgramStore(owner_proxy.address)
  pstore = await SGXProgramStore.at(tx.logs[0].args.addr)
  sr.write("program-store", pstore.address)

  cny = await ERC20Token.at(paysys.read('CNY'))
  //console.log("creating trust list...")
  //tlist_factory = await dhelper.readOrCreateContract(TrustListFactory, [AddressArray]);
  //tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
  //token_trustlist = await TrustList.at(tx.logs[0].args.addr);
  //sr.write("token_tl", token_trustlist.address);

  //token_factory = await dhelper.readOrCreateContract(ERC20TokenFactory, [SafeMath, AddressArray]);
  //console.log("creating token...")
  //tx = await token_factory.createCloneToken('0x0000000000000000000000000000000000000000', 0, "Chinese Yuan", 6, "CNY", true);
  //token = await ERC20Token.at(tx.logs[0].args._cloneToken);
  //await token.changeTrustList(token_trustlist.address)
  //await token_trustlist.add_trusted(accounts[0])
  //sr.write("CNY", token.address);
  //cny = token
  //end creating cny


  tlist_factory = await dhelper.readOrCreateContract(TrustListFactory, [AddressArray]);
  tx = await tlist_factory.createTrustList(['0x0000000000000000000000000000000000000000']);
  tlist = await TrustList.at(tx.logs[0].args.addr);
  sr.write('market-tlist', tlist.address)

  market=await dhelper.readOrCreateContract(SGXStaticDataMarketPlace,[SafeMath,SafeERC20,ECDSA,SignatureVerifier,SGXStaticData],pstore.address,owner_proxy.address,cny.address)
  await market.changeTrustList(tlist.address)
  sr.write("market", market.address)
  await market.changeFee('100000')
  await market.changeFeePool(accounts[0])


  onchain_impl = await dhelper.readOrCreateContract(SGXOnChainResultMarketImplV1, [SafeMath, ECDSA, SignatureVerifier, SGXStaticData])
  sr.write('onchain-market-impl', onchain_impl.address)
  onchain_market = await dhelper.readOrCreateContract(SGXOnChainResultMarket, [SafeERC20])
  sr.write('onchain-market', onchain_market.address)
  await onchain_market.changeDataLib(onchain_impl.address)
  await onchain_market.changeMarket(market.address)
  await tlist.add_trusted(onchain_market.address)

  common_impl = await dhelper.readOrCreateContract(SGXDataMarketCommonImplV1, [SignatureVerifier, SGXStaticData])
  common_market = await dhelper.readOrCreateContract(SGXDataMarketCommon, [])
  sr.write('common-market', common_market.address)
  sr.write('common-market-impl', common_impl.address)

  await common_market.changeDataLib(common_impl.address)
  await common_market.changeMarket(market.address)
  await tlist.add_trusted(common_market.address)

  pay_proxy = paysys.read('paypool')
  await onchain_market.changeConfirmProxy(pay_proxy)
  await common_market.changeConfirmProxy(pay_proxy)
  ppool_tlist = await TrustList.at(paysys.read("pool_tl"))
  await ppool_tlist.add_trusted(onchain_market.address)
  await ppool_tlist.add_trusted(market.address)
  await ppool_tlist.add_trusted(common_market.address)

}

module.exports = function(deployer, network, accounts){
  deployer
    .then(function() {
      console.log(DHelper)
      return performMigration(deployer, network, accounts, DHelper(deployer, network, accounts))
    })
    .catch(error => {
      console.log(error)
      process.exit(1)
    })
};
